import numpy

def feedback_ADU_to_A(val, fb_bits=14, fb_maxV=.965, fb_R=15200, m=8):

    b = fb_bits
    v = fb_maxV
    r = fb_R

    val = val * (2.0**-b) * v / (r * m)

    return val

def bias_ADU_to_A(val, bias_bits=16, bias_maxV=2.5, bias_R=1096):

    b = bias_bits
    v = bias_maxV
    r = bias_R

    val = val * (2.0**-b) * v / r

    return val

def bias_ADU_to_V(val, bias_bits=16, bias_maxV=2.5, bias_R=1096):

    b = bias_bits
    v = bias_maxV

    val = val * (2.0**-b) * v

    return val

def feedback_A_to_ADU(val, fb_bits=14, fb_maxV=.965, fb_R=1050, m=8):

    b = fb_bits
    v = fb_maxV
    r = fb_R

    val = (val * r * m / v) * (2.0**b)

    return val

def bias_A_to_ADU(val, bias_bits=16, bias_maxV=2.5, bias_R=1096):

    b = bias_bits
    v = bias_maxV
    r = bias_R

    val = (val * r / v) * (2.0**b)

    return val

def bias_V_to_ADU(val, bias_bits=16, bias_maxV=2.5, bias_R=1096):

    b = bias_bits
    v = bias_maxV

    val = (val / v) * (2.0**b)

    return val



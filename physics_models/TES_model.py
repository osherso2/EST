import numpy
import matplotlib.pyplot as plt
#from scipy.optimize import minimize

hbar = 6.58212e-16 # eV s
kb = 8.61733e-5 # eV / K

class TES:

    def __init__(self, Tc, Ic, Rn, alpha0, I, T, name):
        """A TES model using a atanh function."""

        self.name = name

        # TES parameters
        self.Tc=float(Tc) # K
        self.Rn=float(Rn) # Ohm
        self.Ic=float(Ic) # A
        self.alpha0=float(alpha0)

        # TES state
        self.set_state(T=T, I=I)

    def get_Ic(self, T=None):
        """Returns critical current at T."""

        if T is None: T = self.T

        Tc = self.Tc
        Ic = self.Ic
        Rn = self.Rn

        if T >= Tc: return 0
        assert(T >= 0)

        return  Ic * numpy.power((Tc - T)/Tc, (3./2.))

    def get_Tc(self, I=None):
        """Returns critical temperature at I."""

        if I is None: I = self.I

        Tc = self.Tc
        Ic = self.Ic
        Rn = self.Rn

        if abs(I) >= self.get_Ic(0): return 0

        return Tc - Tc*numpy.power(abs(I)/Ic, 2./3.)

    def get_R(self, I=None, T=None):
        """
        Returns resistance at given I, T.
        Using tanh function as a model.
        """

        if I is None: I = self.I
        if T is None: T = self.T

        Rn = self.Rn
        Tc = self.Tc
        alpha0 = self.alpha0

        DeltaT = Tc / alpha0
        TcI = self.get_Tc(I)
        return (1. + numpy.tanh((T - TcI)/DeltaT))*Rn/2.

    def get_I(self, R=None, T=None):
        """Returns the current at a given R, T."""

        if R is None: R = self.R
        if T is None: T = self.T

        Rn = self.Rn
        Tc = self.Tc
        alpha0 = self.alpha0

        DeltaT = Tc / alpha0

        TcI = T - (DeltaT * numpy.arctanh((2.*R/Rn) - 1.))
        return self.get_Ic(TcI)

    def get_T(self, R=None, I=None):
        """Returns the temperature at a given R, I."""

        if R is None: R = self.R
        if I is None: I = self.I

        Rn = self.Rn
        Tc = self.Tc
        alpha0 = self.alpha0

        DeltaT = Tc / alpha0

        TcI = self.get_Tc(I=I)
        return (DeltaT * numpy.arctanh((2.*R/Rn) - 1.)) + TcI

    def get_alpha(self, I=None, T=None):
        """Returns the alpha at a given I, T."""

        if I is None: I = self.I
        if T is None: T = self.T

        assert(T >= 0)

        Rn = self.Rn
        Tc = self.Tc
        alpha0 = self.alpha0

        R = self.get_R(I=I, T=T)

        return 2.*alpha0*(T/Tc)*(1. - R/Rn)

    def get_beta(self, I=None, T=None):
        """Returns the beta at a given I, T."""

        if I is None: I = self.I
        if T is None: T = self.T

        assert(I >= 0)
        assert(T >= 0)

        if I > 2*self.Ic: return 0
        if T > 2*self.Tc: return 0

        Rn = self.Rn
        Tc = self.Tc
        Ic = self.Ic

        alpha0 = self.alpha0

        R = self.get_R(I=I, T=T)
        r = R / Rn

        pfactor = alpha0 * 4. / 3. * (I/Ic)**(2./3.)

        return pfactor * (1. - r)

    def set_state(self, I=None, T=None):
        """
        Uses the above functions to set
        a correct state with R(I, T).
        """

        if T is None: T = self.T
        if I is None: I = self.I

        R = self.get_R(I=I, T=T)

        self.R = R
        self.I = I
        self.T = T

    def print_state(self, prepend=""):

        print prepend+"name", self.name
        print prepend+"Rn", self.Rn
        print prepend+"Tc", self.Tc
        print prepend+"Ic", self.Ic
        print prepend+"alpha0", self.alpha0
        print prepend+"---"
        print prepend+"R", self.R
        print prepend+"I", self.I
        print prepend+"T", self.T

    def copy_TES(self):
        """Make a copy of current TES"""

        return TES(Tc=self.Tc, Ic=self.Ic, Rn=self.Rn, name=self.name,
                    alpha0=self.alpha0, I=self.I, T=self.T)

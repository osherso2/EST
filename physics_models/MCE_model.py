import time as timer

import scipy.signal as scs
import numpy

from adu_conversion import feedback_ADU_to_A as fb_A
from adu_conversion import feedback_A_to_ADU as fb_ADU
from adu_conversion import bias_ADU_to_A as b_A
from adu_conversion import bias_A_to_ADU as b_ADU

# constants
eV = 1.6e-19 # J
keV = 1000*eV # J
k = 1.38e-23 # J/K

class MCE:

    def __init__(self, row_len, data_rate, num_rows, data_mode,
            filter_coeff, P, I, D, closed_loop,
            transfer, invtrans, feedback_params, bias_params):

        self.Bphi0 = None

        self.transfer = transfer
        self.invtrans = invtrans

        self.P = P
        self.I = I
        self.D = D

        self.feedback_params = feedback_params
        self.bias_params = bias_params
        self.b_A = lambda v: b_A(v, **bias_params)
        self.b_ADU = lambda v: b_ADU(v, **bias_params)
        self.fb_A = lambda v: fb_A(v, **feedback_params)
        self.fb_ADU = lambda v: fb_ADU(v, **feedback_params)

        self.sum_err = 0
        self.cor = 0

        self.find_Bphi0()
        self.find_Bphi0()

        # digital filter
        self.make_filter(filter_coeff)

        self.row_len = row_len
        self.data_rate = data_rate
        self.num_rows = num_rows
        self.closed_loop=closed_loop

        if data_mode not in [0, 1, 10]:
            print "Only coded in data modes 0, 1 and 10. Setting data_mode = 10."
            self.data_mode = 10

        else: self.data_mode = data_mode 

        self.get_rate()

    def get_rate(self):

        rate = 5e7 / self.row_len
        rate = rate / self.num_rows
        self.mux_rate = rate
        rate = rate / self.data_rate
        self.rate = rate
        return rate

    def find_Bphi0(self):

        x = numpy.arange(2.**22)
        if self.Bphi0 is not None:
            x-= 2.**21
            x = x / 2.**22
            x = x * self.Bphi0 * 4.
        else: 
            x-= 2.**21
            x = x / 2.**6
        y = self.transfer(x)
        m = numpy.mean(y)
        b = numpy.polyval(numpy.polyfit(x, y, 1), x)
        amps = numpy.fft.fft(y*numpy.hanning(len(y)))
        freq = numpy.fft.fftfreq(len(y)) / (x[1] - x[0])
        major = freq[numpy.argmax(numpy.abs(amps))]

        self.Bphi0 = numpy.abs(1./major)
        self.fb0 = self.invtrans(m)

    def make_filter(self, filter_coeff):

        assert(len(filter_coeff) == 6)

        self.filter_coeff = filter_coeff
        b  = [1., 2., 1.]
        a1 = [1., -filter_coeff[0]/2.**14, filter_coeff[1]/2.**14]
        a2 = [1., -filter_coeff[2]/2.**14, filter_coeff[3]/2.**14]

        K = 2.**-14
        scalars = [K, K, K, K, 1., 1.]
        b11, b12, b21, b22, k1, k2 = [s*p for s,p in zip(scalars, filter_coeff)]

        gain = (2.)**4 / (1. - b11 + b12) / (1. - b21 + b22)
        gain /= 2**(k1+k2)
        self.gain = gain

        z1 = scs.lfilter_zi(b, a1)
        z2 = scs.lfilter_zi(b, a2)

        def mcefilter(data, a, b, g, z):

            res  = scs.lfilter(b, a, data, zi=z)
            filt = res[0] / 2**g
            filt = numpy.floor(filt)

            return filt, res[1]

        def twostages(data, a1=a1, a2=a2, g1=filter_coeff[5], g2=filter_coeff[4], b1=b, b2=b, z1=z1, z2=z2):

            filt1, zf1 = mcefilter(data,  a1, b1, g1, z1)
            filt2, zf2 = mcefilter(filt1, a2, b2, g2, z2)

            return filt2, zf1, zf2

        self.mcefilter = lambda x : twostages(x)[0]

        return twostages

    def sim(self, time, data):

        time = numpy.array(time)
        data = numpy.array(data)

        transfer = self.transfer
        invtrans = self.invtrans
        closed_loop = self.closed_loop

        num_rows = self.num_rows
        row_len = self.row_len
        N = len(data) / (row_len * num_rows)

        P = self.P
        I = self.I
        D = self.D

        old_err = 0
        sum_err = self.sum_err
        cor = self.cor

        data = self.fb_ADU(data)

        T      = []
        FB     = []
        ERR    = []

        for i in range(1, N-1):

            inp = data[(i*(row_len*num_rows))-10:i*(row_len*num_rows)]
            phi = inp - (cor >> 12)
            err = numpy.sum((transfer(phi - self.fb0)/10.).astype('int'))

            sum_err += err
            der_err = old_err - err
            old_err = err

            if closed_loop: 
                cor = int(P * err) + int(I * sum_err) + int(D * der_err)

            T.append(time[((i+1)*(num_rows*row_len))])
            FB.append(cor)
            ERR.append(err)

        self.sum_err = sum_err
        self.cor = cor

        return numpy.array(T), numpy.array(FB), numpy.array(ERR)

    def interp_to_50MHz(self, time, data):

        dt = 1. / 5e7

        t = numpy.arange(time[0], time[-1], dt)
        d = numpy.interp(t, time, data)

        return t, d

    def apply_data_mode(self, time, data, unfactor=True, offset_frac=0.):

        offset = offset_frac / self.rate
        data_rate = self.data_rate
        data_mode = self.data_mode

        time = numpy.array(time)
        data = numpy.array(data)

        if numpy.abs(1./(time[1] - time[0]) - 5e7) > 1: time, data = self.interp_to_50MHz(time, data)
        t, d, e = self.sim(time[time > offset], data[time > offset])
        t += offset

        if data_mode == 0: 
            return t[::data_rate], d[::data_rate], e[::data_rate]
        if data_mode == 1: 
            if unfactor: d = d/(2.**12)
            return t[::data_rate], d[::data_rate], e[::data_rate]
        if data_mode >= 2:
            d = numpy.append([d[0]]*12000, d)
            d = self.mcefilter(d)/self.gain
            d = numpy.array(d).astype('int')[12000:]
            if unfactor: d = d/(2.**12)
            return t[::data_rate], d[::data_rate], e[::data_rate]

import os, sys
import numpy
import pickle
import datetime
from scipy.optimize import minimize

from TES_model import *

hbar = 6.58212e-16 # eV s
kb = 8.61733e-5 # eV / K
J_eV = 6.2415091e18 # eV / J

nan = numpy.nan

def load_pkl(filename=None, state=None):

    if filename is None:
        assert(not state is None)
    else:
        with open(filename, 'r') as loadstate:
            state = pickle.load(loadstate)

    para = {}; TESn = []; Tcs = []
    for k in state.keys():
        if "tes" in k:
            TESn.append(k)
            Tcs.append(state[k]["Tc"])
            continue
        para[k] = state[k]

    TESs = []
    for tes in TESn: TESs.append(TES(**state[tes]))
    TESs = numpy.array(TESs)[numpy.argsort(Tcs)]

    para["TESs"] = TESs
    return detector(**para)

class detector:

    """A model of our island/TES based detectors"""

    def __init__(self,TESs,Tx,Cx,Gx,betaC,betaG,num_rows,Rsh,Rb,Rl,Rs,
                 L,Vb,Tb,R,I,T,Popt,center_freq,bandwidth):
        """
        DEVICE DESCRIPTION
        TESs: list of TES objects.
           This MUST be in order of first superconducting to last!
        Tx [K]: float
           Reference temperature on which to measure heat capacity and
           thermal conductance.
        Cx [J/K]: float
           Heat capacity of the island at Tx.
        Gx [W/K]: float
           Thermal conductance of the island at Tx.
        betaC: float
           Power law parameter: C(T) = Cx(T/Tx)**betaC.
        betaG: float
           Power law parameter: G(T) = Gx(T/Tx)**betaG.
        num_rows: int
           Number of detectors in a row (to compute load on bias).
        Rsh [Ohms]: float
           Shunt resistor.
        Rb [Ohms]: float
           Bias resistor.
        Rl [Ohms]: float
           Parasitic resistance on the island.
        Rs [Ohms]: float
           Stray resistance off the island.
        L [H]: float
           Inductance coupled to the TESs.
        DEVICE STATE
        Vb [V]: float
           Bias voltage.
        Tb [K]: float
           FPU temperature.
        R [Ohms]: float
           Resistance on the island.
        I [A]: float
           Current through the island.
        T [K]: float
           Temperature of the island.
        LOADING
        Popt [W]: float
           Optical loading.
        center_freq [Hz]: float
           Center frequency of the band.
        bandwidth [Hz]: float
           Width of the band.
        """
        # Thermal
        self.Tx=float(Tx) # [K] The temp at which Cx and Gx measured.
        self.Cx=float(Cx) # [J/K] Island heat capacity at Tx.
        self.Gx=float(Gx) # [W/K] Leg heat conductance at Tx.
        self.betaC=float(betaC) # Power law heat cap: C = Cx * (T/Tx)**betaC.
        self.betaG=float(betaG) # Power law leg cond: G = Gx * (T/Tx)**betaG.
        # Electrical
        self.num_rows = num_rows # To know the load on the bias voltage.
        self.L=float(L) # [H] Inductance.
        self.Rsh=float(Rsh) # [Ohm] Shunt resistor.
        self.Rl=float(Rl) # [Ohm] Parasitic resistance on the island.
        self.Rs=float(Rs) # [Ohm] Stray resistance NOT on the island.
        self.Rb=float(Rb) # [Ohm] Bias resistor.
        self.Rn = Rl
        for tes in TESs: self.Rn += tes.Rn # Possible res on the island.
        # Environment / Loading
        self.Popt=float(Popt) # [W] Optical loading.
        self.center_freq = center_freq # [Hz] Center of the band.
        self.bandwidth = bandwidth # [Hz] Width of the band.

        # State
        self.Vb = Vb # [V] Bias of entire row (not voltage across the TES).
        self.Tb = Tb # [K] Temperature of FPU (other side of the legs).
        self.R = R # [Ohms] Resistance of the island (Rs of all TESs + Rl).
        self.I = I # [A] Current flowing through the TESs.
        self.T = T # [K] Temperature of the island.

        # Copying each TES onto the island.
        self.TESs = []
        for tes in TESs:
            tes_copy = tes.copy_TES()
            tes_copy.set_state(I=I, T=T)
            self.TESs.append(tes_copy)

    def set_state(self, I=None, T=None):
        """
        Sets the state. R=R(I, T), Vb=Vb(R, I).
        If I or T is None, uses current values.
        """

        if I is None: I = self.I
        self.I = max(I, 0.)
        if T is None: T = self.T
        self.T = max(T, 0.)

        self.R = self.get_R(I=I, T=T)
        self.Vb = self.get_Vb(I=I)

        for tes in self.TESs: tes.set_state(I=I, T=T)
        self.sim([0, 1e-12])

    def get_Vb(self, R=None, I=None):
        """
        Get the bias voltage required to get
        the given I for the given R.
        If I or R is None, uses current values.
        """

        if R is None: R = self.R
        if I is None: I = self.I

        Rsh = self.Rsh
        Rs = self.Rs
        Rb = self.Rb
        N = self.num_rows

        Rc = N*(R + Rs)*Rsh/(R + Rs + Rsh)
        Ib = I * (Rsh + Rs + R) / Rsh
        return Ib * (Rb + Rc)

    def get_Ib(self, Vb=None, R=None):
        """
        Get the bias current given Vb and R.
        the given I for the given R.
        If Vb or R is None, uses current values.
        """

        if Vb is None: Vb = self.Vb
        if R is None: R = self.R

        Rsh = self.Rsh
        Rs = self.Rs
        Rb = self.Rb
        N = self.num_rows

        Rc = N*(R + Rs)*Rsh/(R + Rs + Rsh)
        Ib = Vb / (Rb + Rc)

        return Ib

    def get_R(self, I=None, T=None):

        if I is None: I = self.I
        if T is None: T = self.T

        R = self.Rl
        for tes in self.TESs: R += tes.get_R(I=I, T=T)

        return R

    def get_I(self, R=None, Vb=None):

        if Vb is None: Vb = self.Vb
        if R is None: R = self.R

        Ib = self.get_Ib(Vb=Vb, R=R)
        I = Ib * self.Rsh / (self.Rsh + self.Rs + R)
        return I

    def get_T(self, R=None, I=None):

        if R is None: R = self.R
        if I is None: I = self.I

        Gx = self.Gx
        Tx = self.Tx
        beta = self.betaG
        eta = beta + 1.
        Tb = self.Tb
        Popt = self.Popt

        A = (Tb**eta)*Gx/(eta * Tx**beta)

        B = R*I**2. + Popt + A
        B*= eta*(Tx**beta)/Gx

        T = B**(1./eta)

        return T

    def get_G(self, T=None):

        if T is None: T = self.T
        return self.Gx * (self.T/self.Tx)**self.betaG

    def get_C(self, T=None):

        if T is None: T = self.T
        return self.Cx * (self.T/self.Tx)**self.betaC

    def get_alpha(self, I=None, T=None):

        if I is None: I = self.I
        if T is None: T = self.T
        detR = self.get_R(I=I, T=T)

        alpha = 0
        for tes in self.TESs:
            tesR = tes.get_R(I=I, T=T)
            alpha += tes.get_alpha(I=I, T=T)*tesR/detR

        return alpha

    def get_beta(self, I=None, T=None):

        if I is None: I = self.I
        if T is None: T = self.T
        R = self.get_R(I=I, T=T)

        beta = 0
        for tes in self.TESs:
            R_frac = tes.get_R(I=I, T=T)/R
            beta += tes.get_beta(I=I, T=T)*R_frac

        return beta

    def get_loop_gain(self):

        G = self.get_G()
        return self.get_alpha() * self.R * self.I**2. / (G * self.T)

    def get_taus(self):

        I = self.I
        T = self.T
        L = self.L
        R = self.get_R(I=I, T=T)

        Rc = self.Rs + self.Rsh

        Tx = self.Tx
        alpha = self.get_alpha()
        beta  = self.get_beta()
        loop_gain = self.get_loop_gain()
        gamma = Rc/R

        G = self.get_G()
        C = self.get_C()

        tau_CG = C/G
        tau_LR = L/R
        tau_el = tau_LR / (1 + beta + gamma)
        tau_tm = tau_CG / (1. - loop_gain)

        disc = ((tau_tm - tau_el)/(tau_tm * tau_el))**2.
        disc-= 4.* loop_gain * (2. + beta) / (tau_LR * tau_CG)
        disc = complex(disc)**.5
        term = (tau_tm + tau_el)/(tau_tm * tau_el)

        tau_p = 2/(term + disc)
        tau_m = 2/(term - disc)

        return tau_p, tau_m, tau_el, tau_tm

    def stable(self):

        tau_p, tau_m, _, _ = self.get_taus()
        if numpy.isnan(tau_p): return False
        if numpy.isnan(tau_m): return False
        if tau_p.real <= 0 or tau_m.real <= 0: return False
        return True

    def overdamped(self):

        tau_p, tau_m, _, _ = self.get_taus()
        if abs(numpy.imag(tau_p)) > 1e-12: return False
        if abs(numpy.imag(tau_m)) > 1e-12: return False
        return self.stable()

    def get_Lcrits(self):

        R = self.R
        Rl = self.Rs + self.Rsh
        gamma = Rl / R
        beta = self.get_beta()
        tau = self.get_C() / self.get_G()
        loop_gain = self.get_loop_gain()

        A = loop_gain*(3. + beta - gamma)
        B = 1. + beta + gamma
        C = loop_gain*(2. + beta)
        D = loop_gain*(1. - gamma)
        E = 2.*numpy.sqrt(C*(D + B))
        F = R*tau/(loop_gain - 1.)**2.
        return F*(A + B + E), F*(A + B - E)

    def get_Lthresh(self):

        R = self.R
        Rl = self.Rs + self.Rsh
        gamma = Rl / R
        beta = self.get_beta()
        tau = self.get_C() / self.get_G()
        loop_gain = self.get_loop_gain()

        prefactor = (1. + beta + gamma) / (loop_gain - 1.)
        return prefactor * R * tau

    def P_in(self, R=None, I=None, T=None):

        if I is None: I = self.I
        if T is None: T = self.T
        if R is None: R = self.get_R(I=I, T=T)

        P_J = R*(I**2.)
        return P_J + self.Popt

    def P_out(self, T=None):

        if T is None: T = self.T

        eta = self.betaG + 1.
        return self.Gx*(T**eta - self.Tb**eta)/(eta * self.Tx**self.betaG)

    def P_bal(self, R=None, I=None, T=None):

        return self.P_in(R=R, I=I, T=T) - self.P_out(T=T)

    def halfway_finder(self, TES_index=0, set_state=True, verbose=False):

        # Make asserts, defined target
        tess = numpy.array(self.TESs)
        assert(TES_index < len(tess))

        target = self.Rl
        for tes in tess[:TES_index]: target += tes.Rn
        tes = tess[TES_index]
        target += tes.Rn/2. # <--- desired R

        # Define the err function to minimize
        fI = lambda T: tes.get_Ic(T=T)
        err = lambda T: (self.P_bal(R=target, I=fI(T), T=T)/1e-20)**2.

        # Make a good first guess
        Tc = tes.Tc
        Tsc = self.get_T(R=0, I=0) # Temp for unbiased detector.
        T_bounds = (Tsc, 1.1*Tc)
        guess = [.99*Tc]

        # Whichever default method minimize chooses works fine.
        options = {"ftol":1e-18, "gtol":1e-18, "eps":1e-12}
        res = minimize(err, guess, bounds=[T_bounds], options=options)

        # This can happen if Popt is high relative to saturation power.
        if "NO FEASIBLE SOLUTION" in res.message:
            msg = "halfway_finder: No feasible solution"
            if verbose: print msg
            return False, nan, nan

        # Find full state details for given solution.
        I = fI(res.x[0]); T = res.x[0]
        R = self.get_R(I=I, T=T)

        # Check that this solution is actually good.
        fail = False
        E = abs(R/target - 1.)
        P = abs(self.P_bal(I=I, T=T))
        if E > .01:
            msg = "target: %.3e Ohms, found: %.3e Ohms (|rel_err|=%03.02f%%)"
            msg = msg % (target, R, 100.*R)
            msg = "halfway_finder: Wrong R found:\n" + msg
            if verbose: print msg
            fail = True
        if abs(P) > 1e-18:
            msg = "halfway_finder: Unsettled: |P_bal| = %.3e W" % P
            if verbose: print msg
            fail = True

        if fail: return False, I, T
        if set_state: self.set_state(I=I, T=T)
        return True, I, T

    def find_bias(self, R_frac=.65, TES_index=0, verbose=False,
                  min_args=None, max_iters=None):

        # Make asserts and define the target.
        assert(R_frac > 0 and R_frac <= 1)
        if TES_index == -1: TES_index = len(self.TESs) - 1
        tess = self.TESs
        assert(TES_index < len(tess))

        Rc = self.Rl
        for tes in tess[:TES_index]: Rc += tes.Rn
        tes = tess[TES_index].copy_TES()
        target = R_frac*tes.Rn + Rc

        ret = self.halfway_finder(TES_index=TES_index, set_state=False)
        if not ret[0]:
            if verbose: print "find_bias: Failed running halfway_finder"
            return False, nan, nan # Failed halfway_finder.
        if verbose: print "find_bias: Succeeded running halfway_finder"
        I, T = ret[1], ret[2]
        if R_frac == .5:
            self.set_state(I=I, T=T)
            return True
        guess = [I, T]

        # My hacky metric for sucess
        def metric(E, P):
            M = 100.*E + 10.*numpy.log10(P*1e18)*(P > 1e-18)
            if numpy.isnan(M): return numpy.inf
            return M

        R = self.get_R(I=I, T=T)
        E = abs(R/target - 1.)
        P = abs(self.P_bal(I=I, T=T))
        M = metric(E, P)

        ret, I, T = self._find_bias1(R_frac=R_frac,
                                     TES_index=TES_index,
                                     guess=guess,
                                     set_state=False,
                                     verbose=verbose,
                                     min_args=min_args)

        if ret:
            self.set_state(I=I, T=T)
            return True

        R = self.get_R(I=I, T=T)
        E = abs(R/target - 1.)
        P = abs(self.P_bal(I=I, T=T))
        nM = metric(E, P)
        if M > nM: guess = [I, T]

        print "switching to method 2"
        ret, I, T = self._find_bias2(R_frac=R_frac,
                                     TES_index=TES_index,
                                     guess=guess,
                                     set_state=False,
                                     verbose=verbose,
                                     max_iters=max_iters)

        if ret:
            self.set_state(I=I, T=T)
            return True

        return False

    def _find_bias1(self, R_frac=.65, TES_index=0,
                         set_state=True, guess=None, verbose=False,
                         min_args=None):

        if min_args is None:
            min_args={"method":"SLSQP",
                      "options":{"maxiter":1e6}}

        # Make asserts and define the target.
        assert(R_frac > 0 and R_frac <= 1)
        if TES_index == -1: TES_index = len(self.TESs) - 1
        tess = self.TESs
        assert(TES_index < len(tess))

        Rc = self.Rl
        for tes in tess[:TES_index]: Rc += tes.Rn
        tes = tess[TES_index].copy_TES()
        target = R_frac*tes.Rn + Rc

        # Define err function to minimize and constraint.
        def err(x):
            I, T = x
            R = tes.get_R(I,T) + Rc
            return (R/target - 1.)**2.

        def constraint(x):
            I, T = x
            R = tes.get_R(I,T) + Rc
            P = self.P_bal(R=R, I=I, T=T)
            return P/1e-18
        my_constraint = ({"type":"eq", "fun":constraint})

        # If no guess is given, use the halfway point.
        ret = self.halfway_finder(TES_index=TES_index, set_state=False)
        halfway_I, halfway_T = ret[1], ret[2]
        if guess is None:
            if not ret[0]: return False, nan, nan # Failed halfway_finder.
            guess = [halfway_I, halfway_T]

        # Determine reasonable I, T bounds
        Tsc = self.get_T(R=0, I=0)
        T_low = .1*self.Tb + .9*Tsc
        if R_frac < .5:
            I_bounds = (0., 100.*halfway_I)
            T_bounds = (T_low, 100.*halfway_T)
        else:
            I_bounds = (0.01*halfway_I, 100.*tes.Ic)
            T_bounds = (max(T_low, 0.01*halfway_T), 100.*tes.Tc)
        bounds = (I_bounds, T_bounds)

        # Run the minimizer.
        res = minimize(err, guess, bounds=bounds,
                       constraints=my_constraint,
                       **min_args)

        # This can happen if Popt is high relative to saturation power.
        if "NO FEASIBLE SOLUTION" in res.message:
            msg = "find_bias1: No feasible solution"
            if verbose: print msg
            return False, nan, nan

        # Interpret results to full state.
        I = res.x[0]; T = res.x[1]

        # Check that this is a good solution.
        fail = False
        R = self.get_R(I=I, T=T)
        E = abs(R/target - 1.)
        P = abs(self.P_bal(I=I, T=T))
        if E > .01:
            msg = "target: %.3e Ohms, found: %.3e Ohms (|rel_err|=%03.02f%%)"
            msg = msg % (target, R, 100.*E)
            msg = "find_bias1: Wrong R found:\n" + msg
            if verbose: print msg
            fail = True
        if P > 1e-18:
            msg = "find_bias1: Unsettled: |P_bal| = %.3e W" % P
            if verbose: print msg
            fail = True

        if fail: return False, I, T
        if set_state: self.set_state(I=I, T=T)
        return not fail, I, T

    def _find_bias2(self, R_frac=.65, TES_index=0, guess=None,
                                set_state=False, verbose=False,
                                max_iters=None):

        assert(R_frac > 0 and R_frac <= 1)

        if TES_index == -1: TES_index = len(self.TESs) - 1

        tess = self.TESs
        assert(TES_index < len(tess))

        Rc = self.Rl
        for tes in tess[:TES_index]: Rc += tes.Rn
        tes = tess[TES_index].copy_TES()
        target = R_frac*tes.Rn + Rc

        def taper(x, scale=1e-2):
            return scale*(1. - numpy.exp(-x/scale))
        def signed_taper(x, scale=1e-2):
            return numpy.sign(x)*taper(abs(x), scale=scale)
        def randomizer(x):
            y = numpy.random.rand()*x
            return y
        error = lambda R: abs(R/target - 1.)

        # If no guess is given, use the halfway point.
        ret = self.halfway_finder(TES_index=TES_index, set_state=False)
        halfway_I, halfway_T = ret[1], ret[2]
        if guess is None:
            if not ret[0]: return False, nan, nan # Failed halfway_finder.
            guess = [halfway_I, halfway_T]
        I, T = guess

        # Determine reasonable I, T bounds
        Tsc = self.get_T(R=0, I=0)
        T_low = .1*self.Tb + .9*Tsc
        if R_frac < .5:
            I_bounds = (1e-12, 1.5*halfway_I)
            T_bounds = (T_low, 1.5*halfway_T)
        else:
            I_bounds = (0.5*halfway_I, 10.*tes.Ic)
            T_bounds = (max(T_low, 0.5*halfway_T), 1.1*tes.Tc)
        bounds = (I_bounds, T_bounds)

        Popt = self.Popt
        betaG = self.betaG

        if max_iters is None: max_iters = 1e4
        i = 0
        P = self.P_bal(I=I, T=T)
        R = self.get_R(I=I, T=T)
        while error(R) > .01 or abs(P) > 1e-18:

            Pout = self.P_out(T=T)
            Pjle = self.P_in(I=I, T=T) - Popt
            D = (target/R - 1)

            betaI = self.get_beta(I=I, T=T)
            alphaI = self.get_alpha(I=I, T=T)

            G = self.Gx*(T/self.Tx)**betaG
            dI = (G*T - alphaI*Pjle)*(D/alphaI) - P
            dI/= (Pjle*(2. + betaI) + (G*T/alphaI - Pjle)*betaI)
            dI*= I

            d = 1e-7
            if abs(P) < 1e-15: d /= 10.
            if abs(P) < 1e-16: d /= 10.
            if abs(P) < 1e-17: d /= 10.
            dI = signed_taper(dI, d)

            dT = (D - betaI*dI/I)/alphaI
            dT*= T

            if i % 1e2 == 0:
                dI = randomizer(dI)
                dT = randomizer(dT)

            I += dI; T += dT
            if I < I_bounds[0]: I = I_bounds[0]
            if T < T_bounds[0]: T = T_bounds[0]

            R = self.get_R(I=I, T=T)
            P = self.P_bal(I=I, T=T)

            i += 1
            if i > max_iters:
                msg = "target: %.3e Ohms, found: %.3e Ohms (%03.02f%%)"
                msg = msg % (target, R, 100.*(R/target - 1.))
                msg = ("find_bias2: Exceeded %e iterations:\n" % max_iters) + msg
                if verbose: print msg
                return False, I, T

        R = self.get_R(I=I, T=T)
        P = self.P_bal(I=I, T=T)

        fail = False
        if numpy.any(numpy.isnan([I, T, R, P])):
            msg = "target: %.3e Ohms, found: %.3e Ohms (%03.02f%%)"
            msg = msg % (target, R, 100.*(R/target - 1.))
            msg = "find_bias2: nans encountered:\n" + msg
            if verbose: print msg
            fail = True
        if error(R) > .01:
            msg = "target: %.3e Ohms, found: %.3e Ohms (%03.02f%%)"
            msg = msg % (target, R, 100.*(R/target - 1.))
            msg = "find_bias2: Wrong R found:\n" + msg
            if verbose: print msg
            fail = True
        if P > 1e-18:
            msg = "find_bias2: Unsettled: |P_bal| = %.3e W" % P
            if verbose: print msg
            fail = True

        if fail: return False, I, T
        if set_state: self.set_state(I=I, T=T)
        return not fail, I, T

    def find_equil(self, Vb, set_state=True, verbose=False, guess=None,
                    min_args={"method":"SLSQP",
                              "options":{"ftol":1e-20,
                                         "eps":1e-15}}):

        """Fast way to change bias and reach equilibrium."""

        # Define terms, func to minimize, constraint
        target = float(Vb)

        def err(x):
            I, T = x
            R = self.get_R(I=I, T=T)
            Vb = self.get_Vb(R=R, I=I)
            return (Vb/target - 1.)**2.

        def constraint(x):
            I, T = x
            R = self.get_R(I=I,T=T)
            P = self.P_bal(R=R, I=I, T=T)
            return P/1e-18
        my_constraint = ({"type":"eq", "fun":constraint})

        # Search for closest halfway point and create guess, bounds
        mid_points = []
        for i in range(len(self.TESs)):
            self.halfway_finder(TES_index=i)
            mid_points.append((self.Vb, self.I, self.T))
        mid_points.append((nan, numpy.inf, numpy.inf))

        sc_accepted = True
        Tsc = self.get_T(R=0, I=0)
        T_low = .25*self.Tb + .75*Tsc
        I_bounds = (0., 1.5*mid_points[0][1])
        T_bounds = (T_low, 1.5*mid_points[0][2])
        for k, (Vb, I, T) in enumerate(mid_points[:-1]):
            nguess = [I, T]
            if target == Vb:
                self.halfway_finder(TES_index=k)
                return True, I, T
            if target > Vb:
                sc_accepted = False
                I_bounds = (I_bounds[1]/3., 1.5*mid_points[k+1][1])
                T_bounds = (T_bounds[1]/3., 1.5*mid_points[k+1][2])
                T_bounds = (max(T_low, T_bounds[0]), T_bounds[1])
            else: break
        bounds = (I_bounds, T_bounds)

        if guess is None:
            guess = nguess

        # Increase parasitic resistance to avoid the SC solution.
        # Each iteration is a good guess for the next, smaller Rl.
        prev_rl = 0
        Rl_arr = [min(self.Rn/1e-2, 1e-6), min(self.Rn/1e-5, 1e-9), 0]
        for rl in Rl_arr:

            self.Rl += rl - prev_rl
            prev_rl = rl
            res = minimize(err, guess, bounds=bounds,
                           constraints=my_constraint,
                           **min_args)
            self.Rl -= prev_rl
            guess = [res.x[0], res.x[1]]

            # This should NEVER happen
            if "NO FEASIBLE SOLUTION" in res.message:
                msg = "find_equil: No feasible solution "
                msg+= "(this should NEVER happen)"
                if verbose: print msg
                return False, I, T

        I = res.x[0]; T = res.x[1]
        R = self.get_R(I=I, T=T)
        Vb = self.get_Vb(R=R, I=I)
        P = self.P_bal(I=I, T=T)

        E = Vb/target - 1.
        fail = False
        if abs(E) > .01:
            msg = "target: %.3e V, found: %.3e V (rel_err=%03.02f%%)"
            msg = msg % (target, Vb, 100.*(Vb/target - 1.))
            msg = "find_equil: Wrong Vb found:\n" + msg
            if verbose: print msg
            fail = True
        if abs(P) > 1e-18:
            msg = "find_equil: Unsettled: P_bal = %.3e W" % P
            if verbose: print msg
            fail = True

        if fail: return False, I, T
        if set_state:
            self.set_state(I=I, T=T)
            self.Vb = target
            self.sim([0, 1e-12])
        return True, I, T

    def settle(self, Vb=None, verbose=False):

        if not Vb is None: self.Vb = Vb
        self.sim([0, 1e-24, 2e-24, 1e-18, 2e-18])
        P = abs(self.P_bal())
        best_state = self.save_pkl()
        best_P = P
        s, _, _ = self.find_equil(self.Vb)
        self.sim([0, 1e-24, 2e-24, 1e-18, 2e-18])
        if s and P > abs(self.P_bal()):
            best_state = self.save_pkl()
            best_P = P
        else: self.load_pkl(state=best_state)
        i = 0; j = 0; N = 1000
        while P > 1e-18:

            i+= 1
            dt = 10.**(-16. - j)
            time = numpy.arange(0, N) * dt
            self.sim(time)
            P = abs(self.P_bal())
            if i > 2e3:
                break
            if P < best_P:
                best_state = self.save_pkl()
                best_P = P
                if j > -4: j -= 1
                N = 1000
            elif P < 2*best_P:
                self.load_pkl(state=best_state)
                P = best_P
                if j < 50: j += 1
                N = (N + 2000)/2
            else:
                if numpy.random.rand() < .5:
                    self.load_pkl(state=best_state)
                    self.find_equil(self.Vb)
                else:
                    self.load_pkl(state=best_state)
                    P = best_P
                if j < 50: j += 1
                if N < 1e4: N += 1000

        fail = False
        if numpy.any(numpy.isnan([self.I, self.T, P])):
            msg = "settle: nans encountered:\n"
            if verbose: print msg
            fail = True
        if abs(P) > 1e-18:
            msg = "settle: Unsettled: P_bal = %.3e W" % P
            if verbose: print msg
            fail = True
        if fail: return False

        return True

    def small_sig(self, dt=5e-8, I=None, T=None, Vb=None, Tb=None):
        """
        Simulates one step (of size dt) in time.
        Input: dt (default 1e-6 seconds)
        Output: dI, dT
        """

        if I is None: I = self.I
        self.I = max(I, 0.)
        if T is None: T = self.T
        self.T = max(T, 0.)

        if Vb is None: Vb = self.Vb
        if Tb is None: Tb = self.Tb

        # Current state
        R = self.get_R(I=I, T=T)

        # Electrical impulse
        Rsh   = self.Rsh
        Rs    = self.Rs
        L     = self.L
        Ib    = self.get_Ib(Vb=Vb, R=R)
        LdIdt = Ib*Rsh - I*(Rsh + R + Rs)
        dI    = LdIdt*dt/L

        # Thermal impulse
        CdTdt = self.P_bal(R=R, I=I, T=T)
        dT    = CdTdt * dt / self.get_C(T=T)

        return dI, dT

    def sim(self, time, I=None, T=None, prog=False):

        Vb = self.Vb
        Tb = self.Tb

        R_arr = []
        I_arr = []
        T_arr = []

        if I is None: I = self.I
        if T is None: T = self.T

        curr_p = -1
        dtime = numpy.diff(time)
        dtime = numpy.append(dtime, dtime[-1])
        for i, dt in enumerate(dtime):

            if prog:
                if (100*i)/len(dtime)>curr_p:
                    print "%03d%%" % int((100*i)/len(dtime))
                curr_p += 10

            sig = self.small_sig(I=I,T=T,dt=dt)
            dI, dT = sig

            I = max(I+dI, 0.0)
            T = max(T+dT, 0.0)

            R = self.get_R(I=I, T=T)
            self.R = R

            R_arr = numpy.append(R_arr, R)
            I_arr = numpy.append(I_arr, I)
            T_arr = numpy.append(T_arr, T)

        R_arr = numpy.array(R_arr)
        I_arr = numpy.array(I_arr)
        T_arr = numpy.array(T_arr)

        return time, R_arr, I_arr, T_arr

    def Tb_variation(self, arr_t, arr_T, time=None, plot=False):

        Vb = self.Vb
        Tb = lambda t: numpy.interp(t, arr_t, arr_T)

        I = self.I
        T = self.T

        R_arr  = []
        I_arr  = []
        T_arr  = []
        T_bath = []

        if time is None:
            dt = 1e-6
            N = (numpy.max(arr_t) - numpy.min(arr_t))/dt
            N = numpy.floor(N)
            time = numpy.arange(N)*dt + arr_t[0]

        dtime = numpy.diff(time)
        dtime = numpy.append(dtime, dtime[-1])

        for i, (dt, t) in enumerate(zip(dtime, time)):

            self.Tb = Tb(t)
            dI, dT = self.small_sig(I=I,T=T,dt=dt)

            I += dI
            T += dT

            R = self.get_R(I=I, T=T)
            self.R = R

            R_arr.append(R)
            I_arr.append(I)
            T_arr.append(T)
            T_bath.append(Tb(t))

        R_arr = numpy.array(R_arr)
        I_arr = numpy.array(I_arr)
        T_arr = numpy.array(T_arr)
        T_bath = numpy.array(T_bath)

        return time, T_bath, R_arr, I_arr, T_arr

    def Vb_variation(self, arr_t, arr_V, custom=None):

        Vb = lambda t: numpy.interp(t, arr_t, arr_V)
        Tb = self.Tb

        I = self.I
        T = self.T

        R_arr  = []
        I_arr  = []
        T_arr  = []
        V_bias = []
        if not custom is None:
            custom_arr = []

        dtime = numpy.diff(arr_t)
        dtime = numpy.append(dtime, dtime[-1])

        for i, (dt, t) in enumerate(zip(dtime, arr_t)):

            self.Vb = Vb(t)
            dI, dT = self.small_sig(I=I,T=T,dt=dt)

            I += dI
            T += dT

            R = self.get_R(I=I, T=T)
            self.R = R

            R_arr.append(R)
            I_arr.append(I)
            T_arr.append(T)
            V_bias.append(Vb(t))
            if not custom is None:
                custom_arr.append(custom(self))

        R_arr = numpy.array(R_arr)
        I_arr = numpy.array(I_arr)
        T_arr = numpy.array(T_arr)
        V_bias = numpy.array(V_bias)
        custom_arr = numpy.array(custom_arr)

        if not custom is None:
            return arr_t, V_bias, R_arr, I_arr, T_arr, custom_arr
        return arr_t, V_bias, R_arr, I_arr, T_arr

    def Edep(self, E):

        c = self.Cx / (self.Tx**self.betaC)
        h = self.betaC + 1.
        T_dep = ((h* E / J_eV / c) + self.T**h)**(1./h)
        self.T = T_dep
        for tes in self.TESs: tes.T = self.T

    def cray(self, E=60e6):

        pre_time = numpy.ones(1000)*1e-5 + numpy.linspace(0, 2e-5, 1000)
        pre_time = numpy.cumsum(pre_time)
        t_arr, R_arr, I_arr, T_arr = self.sim(time=pre_time)

        time = [8e-6]*50000; time = numpy.cumsum(time); time = time**2
        time = numpy.append(time, time[-1] + (1. + numpy.arange(1000))*5e-5)
        time = numpy.append(time, time[-1] + (1. + numpy.arange(2000))*1e-4)

        c = self.Cx / (self.Tx**self.betaC)
        h = self.betaC + 1.
        T_dep = ((h* E / J_eV / c) + self.T**h)**(1./h)
        self.T = T_dep
        for tes in self.TESs: tes.T = self.T

        time, R, I, T = self.sim(time=time)
        #return time, R, I, T
        t_arr = numpy.append(pre_time, time+pre_time[-1])
        R_arr = numpy.append(R_arr, R)
        I_arr = numpy.append(I_arr, I)
        T_arr = numpy.append(T_arr, T)
        return t_arr, R_arr, I_arr, T_arr

    def eq_loadcurve(self, Vb_max=None, Vb_min=None, Vb_step=-2e-2, verbose=False):

        truncate = False
        if Vb_max is None: Vb_max  = self.Vb
        if Vb_min is None: Vb_min  = 0; truncate = True

        assert(Vb_max > 0)
        assert(Vb_min < Vb_max)

        self.settle(Vb_max)

        Vb = numpy.arange(Vb_max, Vb_min, Vb_step)
        Vb = numpy.append(Vb[0], Vb)
        Vb = numpy.append(Vb, Vb[-1])

        V = []
        R = []
        I = []
        T = []
        t1 = []
        t2 = []
        prev_vb = Vb_max
        for i, vb in enumerate(Vb):

            if verbose and i % 10 == 0:
                print("V_bias: %.04e V" % vb)
            try:
                self.settle(vb)
            except Exception as e:
                print(e)
                self.settle(prev_vb)
                continue
            prev_vb = vb
            V.append(vb)
            R.append(self.R)
            I.append(self.I)
            T.append(self.T)

            tau_p, tau_m, _, _ = self.get_taus()
            t1.append(tau_p)
            t2.append(tau_m)

            if self.R < 1e-3 * self.TESs[0].Rn:
                if truncate:
                    Vb = Vb[:len(R)]
                    break

        V = numpy.array(V)
        R = numpy.array(R)
        I = numpy.array(I)
        T = numpy.array(T)
        t1 = numpy.array(t1)
        t2 = numpy.array(t2)
        return V, R, I, T, t1, t2

    def print_state(self):
        """
        Prints the current state of the TES in a human readable way maybe.
        """

        print "Tx", self.Tx
        print "Cx", self.Cx
        print "Gx", self.Gx
        print "betaC", self.betaC
        print "betaG", self.betaG
        print "current C", self.get_C()
        print "current G", self.get_G()
        print "current alphaI", self.get_alpha()
        print "current betaI", self.get_beta()
        print "num_rows", self.num_rows
        print "L", self.L
        print "Rsh", self.Rsh
        print "Rb", self.Rb
        print "Rl", self.Rl
        print "Rs", self.Rs
        print "Popt", self.Popt
        print "center_freq %e" % self.center_freq
        print "bandwidth %e" % self.bandwidth
        print "---"
        print "Vb", self.Vb
        print "Tb", self.Tb
        print "R", self.R
        print "I", self.I
        print "T", self.T

        Rc = self.Rl
        TES_index=0
        for i, tes in enumerate(self.TESs):
            print
            tes.print_state(prepend="TES%d  " % i)
            if self.R > tes.Rn:
                TES_index+=1
                Rc+= tes.Rn
        print
        print "P_bal", self.P_bal()
        residual_R = self.R - Rc
        R_perc = 100. * residual_R / self.TESs[TES_index].Rn
        print "R_frac %03.02f%% on TES_index %d" % (R_perc, TES_index)

    def save_py(self, filename=None, overwrite=True):

        if filename is None: filename = sys.stdout
        elif not filename[-3:] == ".py": filename += ".py"

        if overwrite: file_obj = open(filename, 'w')
        else: file_obj = open(filename, 'a')

        dt = str(datetime.datetime.now())
        file_obj.write("# Detector saved on %s\n\n" % dt)

        file_obj.write("from TES_model import *\n")
        file_obj.write("from DET_model import *\n\n")

        file_obj.write("nan = numpy.nan\n\n")

        file_obj.write("# TESs\n")
        file_obj.write("TESs = []; Tcs = []\n")
        for i, tes in enumerate(self.TESs):
            file_obj.write("tes%02d_params = {}\n" % i)
            file_obj.write("tes%02d_params[\"name\"] = \"%s\"" % (i, tes.name))
            file_obj.write(" # name\n")
            file_obj.write("tes%02d_params[\"Ic\"] = %e" % (i, tes.Ic))
            file_obj.write(" # crit current [A]\n")
            file_obj.write("tes%02d_params[\"Tc\"] = %e" % (i, tes.Tc))
            file_obj.write(" # crit temp [K]\n")
            file_obj.write("tes%02d_params[\"Rn\"] = %e" % (i, tes.Rn))
            file_obj.write(" # normal res [Ohms]\n")
            file_obj.write("tes%02d_params[\"alpha0\"] = %e" % (i, tes.alpha0))
            file_obj.write(" # (dln(R)/dln(T) | T=Tc,I=0)\n")
            file_obj.write("# State of TES (at moment of save)\n")
            file_obj.write("tes%02d_params[\"I\"] = %e" % (i, self.I))
            file_obj.write(" # [A]\n")
            file_obj.write("tes%02d_params[\"T\"] = %e" % (i, self.T,))
            file_obj.write(" # [K]\n")
            file_obj.write("tes%02d = TES(**tes%02d_params)\n" % (i,i))
            file_obj.write("TESs.append(tes%02d)\n" % i)
            file_obj.write("Tcs.append(%e)\n" % tes.Tc)
        file_obj.write("TESs = numpy.array(TESs)[numpy.argsort(Tcs)]\n\n")

        file_obj.write("# Detector\n")
        file_obj.write("det_params = {}\n")
        file_obj.write("det_params[\"TESs\"] = TESs\n")
        file_obj.write("det_params[\"Tx\"] = %e" % self.Tx)
        file_obj.write(" # Reference temp for C, G [K]\n" )
        file_obj.write("det_params[\"Cx\"] = %e" % self.Cx)
        file_obj.write(" # Heat capacity at Tx [J/K]\n")
        file_obj.write("det_params[\"Gx\"] = %e" % self.Gx)
        file_obj.write(" # Therm conductance at Tx [W/K]\n")
        file_obj.write("det_params[\"betaC\"] = %e" % self.betaC)
        file_obj.write(" # Exponent on heat capacity\n")
        file_obj.write("det_params[\"betaG\"] = %e" % self.betaG)
        file_obj.write(" # Exponent on therm conductivity\n")
        file_obj.write("det_params[\"num_rows\"] = %d" % self.num_rows)
        file_obj.write(" # Number of equiv dets per bias line\n")
        file_obj.write("det_params[\"L\"] = %e" % self.L)
        file_obj.write(" # Inductance facing det [H]\n")
        file_obj.write("det_params[\"Rsh\"] = %e" % self.Rsh)
        file_obj.write(" # Shunt resistance [Ohms]\n")
        file_obj.write("det_params[\"Rb\"] = %e" % self.Rb)
        file_obj.write(" # Bias resistor [Ohms]\n")
        file_obj.write("det_params[\"Rl\"] = %e" % self.Rl)
        file_obj.write(" # Parasitic res on island [Ohms]\n")
        file_obj.write("det_params[\"Rs\"] = %e" % self.Rs)
        file_obj.write(" # Parasitic res off island [Ohms]\n")
        file_obj.write("det_params[\"Popt\"] = %e" % self.Popt)
        file_obj.write(" # Optical power on island [W]\n")
        file_obj.write("det_params[\"center_freq\"] = %e" % self.center_freq)
        file_obj.write(" # Center freq of observing band [Hz]\n")
        file_obj.write("det_params[\"bandwidth\"] = %e" % self.bandwidth)
        file_obj.write(" # Width of observing band [Hz]\n")
        file_obj.write("# State of det (at moment of save)\n")
        file_obj.write("det_params[\"Vb\"] = %e" % self.Vb)
        file_obj.write(" # Bias voltage [V]\n")
        file_obj.write("det_params[\"Tb\"] = %e" % self.Tb)
        file_obj.write(" # Substrate temp [K]\n")
        file_obj.write("det_params[\"R\"] = %e" % self.R)
        file_obj.write(" # Total island res [Ohms]\n")
        file_obj.write("det_params[\"I\"] = %e" % self.I)
        file_obj.write(" # [A]\n")
        file_obj.write("det_params[\"T\"] = %e" % self.T)
        file_obj.write(" # [K]\n")
        file_obj.write("det = detector(**det_params)")

        file_obj.flush()
        file_obj.close()

    def save_pkl(self, filename=None):
        """
        Saves the state of the detector in a pkl.
        """

        state = {}
        for i, tes in enumerate(self.TESs):
            state["tes_%02d"%i] = {}
            state["tes_%02d"%i]["name"] = tes.name
            state["tes_%02d"%i]["Ic"] = tes.Ic
            state["tes_%02d"%i]["Tc"] = tes.Tc
            state["tes_%02d"%i]["Rn"] = tes.Rn
            state["tes_%02d"%i]["alpha0"] = tes.alpha0
            state["tes_%02d"%i]["I"] = tes.I
            state["tes_%02d"%i]["T"] = tes.T
        state["Tx"] = self.Tx
        state["Cx"] = self.Cx
        state["Gx"] = self.Gx
        state["betaC"] = self.betaC
        state["betaG"] = self.betaG
        state["num_rows"] = self.num_rows
        state["L"] = self.L
        state["Rsh"] = self.Rsh
        state["Rb"] = self.Rb
        state["Rl"] = self.Rl
        state["Rs"] = self.Rs
        state["Popt"] = self.Popt
        state["center_freq"] = self.center_freq
        state["bandwidth"] = self.bandwidth
        state["Vb"] = self.Vb
        state["Tb"] = self.Tb
        state["R"] = self.R
        state["I"] = self.I
        state["T"] = self.T

        if not filename is None:
            with open(filename, 'w') as savestate:
                pickle.dump(state, savestate)

        return state

    def load_pkl(self, filename=None, state=None):

        if filename is None:
            assert(not state is None)
        else:
            with open(filename, 'r') as loadstate:
                state = pickle.load(loadstate)

        para = {}; TESn = []; Tcs = []
        for k in state.keys():
            if "tes" in k:
                TESn.append(k)
                Tcs.append(state[k]["Tc"])
                continue
            para[k] = state[k]

        TESs = []
        for tes in TESn: TESs.append(TES(**state[tes]))
        TESs = numpy.array(TESs)[numpy.argsort(Tcs)]

        para["TESs"] = TESs
        return detector(**para)

    def copy_TESs(self):

        TESs_copy = []
        for tes in self.TESs:
            TESs_copy.append(tes.copy_TES())
        return TESs_copy

    def copy_detector(self):

        return detector(TESs=self.copy_TESs(), num_rows=self.num_rows,
            Tx=self.Tx, Cx=self.Cx, Gx=self.Gx,
            betaC = self.betaC, betaG=self.betaG,
            L=self.L, Rsh=self.Rsh, Popt=self.Popt,
            Rl=self.Rl, Rs=self.Rs, Rb=self.Rb,
            center_freq=self.center_freq,
            bandwidth=self.bandwidth,
            Vb=self.Vb, Tb=self.Tb,
            R=self.R, I=self.I, T=self.T)



import os, sys, re
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
font = {'size' : 12}
from matplotlib import rc
rc('font', **font)

hbar = 1.05457148e-34   # J*s
kb = 1.3806503e-23      # J/K
dpi = 2.*np.pi

def quad_add(*args):
    def func(w):
        res = sum(np.absolute(a(w))**2. for a in args[:])
        return res**.5
    return func

def butterworthed(NEI, cut_off=60., n_poles=4):

    return lambda f: NEI(f)/(1. + (f/cut_off)**(2.*n_poles))

def alias(NEI, new_fs, old_fs):

    assert(old_fs > new_fs)
    nyq_old = old_fs/2.
    nyq_new = new_fs/2.

    def aliased(f):
        N_set = np.arange(np.ceil((-nyq_old-f)/new_fs), np.floor((nyq_old-f)/new_fs)+1)
        f_set = np.abs(f + N_set*new_fs)
        return (f <= nyq_new)*np.sum(np.absolute(NEI(f_set))**2.)**.5

    return np.vectorize(aliased)

def band_rms(NEI, band=[1e-3, 30.], N=10000):

    band_min = band[0]; band_max = band[1]
    assert(band_min < band_max)
    f_arr = np.linspace(band_min, band_max, N)
    return np.trapz(np.nan_to_num(np.absolute(NEI(f_arr)))**2., f_arr)**.5

plot_colors = {}
plot_colors["readout"] = "tab:blue"
plot_colors["photon"] = "tab:orange"
plot_colors["phonon"] = "tab:green"
plot_colors["johnson_sh"] = "tab:red"
plot_colors["johnson_det"] = "tab:purple"
plot_colors["excess"] = "tab:brown"
plot_colors["total"] = "tab:pink"

class noise:

    # Need a few extra parameters to model readout

    def __init__(self, det, readout_plateau, pink_knee, roll_off, m, revisit_rate):

        self.det = det
        self.pink_knee = pink_knee
        self.roll_off = roll_off
        self.plateau = readout_plateau
        self.m = m

        self.multi_f = revisit_rate
        self.multi_w = revisit_rate * dpi
        self.max_f = self.roll_off * 5. # extra factor for extra padding in aliasing
        self.max_w = self.max_f * dpi

        self.S = None

    def get_Fp(self):

        det = self.det

        T = det.T
        Tb = det.Tb
        betaG = det.betaG

        b1 = betaG + 1.; b2 = 2.*betaG + 3.
        Fp = (b1/b2) * (1. - (Tb/T)**b2)/(1. - (Tb/T)**b1)

        return Fp

    ### Responsivity stuff ###

    def responsivity_matrix(self):

        det = self.det
        R = det.R
        I = det.I
        L = det.L
        Rsh = det.Rsh
        G = det.get_G()
        C = det.get_C()
        beta = det.get_beta()
        loop_gain = det.get_loop_gain()

        t0 = C/G; tel = L/R
        M11 = lambda w: G*(1. - loop_gain + 1.j*w*t0)
        M12 = lambda w: -I*R*(2. + beta)*((w+1.)/(w+1.))
        M21 = lambda w: G*loop_gain*((w+1.)/(w+1.))
        M22 = lambda w: I*(Rsh + R*(1. + beta))*(1. + 1.j*w*tel)
        M = (M11, M12, M21, M22)

        prefactor = lambda w: 1./(M11(w)*M22(w) - M12(w)*M21(w))
        S11 = lambda w:  prefactor(w)*M22(w)
        S12 = lambda w: -prefactor(w)*M12(w)
        S21 = lambda w: -prefactor(w)*M21(w)
        S22 = lambda w:  prefactor(w)*M11(w)
        S = (S11, S12, S21, S22)

        self.S = S
        return M, S

    def M_response(self, dI, dT):

        M, _ = self.responsivity_matrix()
        M11, M12, M21, M22 = M
        dPj = lambda w: M21(w)*dT(w) + M22(w)*dI(w)
        dPq = lambda w: M11(w)*dT(w) + M12(w)*dI(w)

        return dPj, dPq

    def S_response(self, dPj, dPq):

        _, S = self.responsivity_matrix()
        S11, S12, S21, S22 = S
        dI = lambda w: S21(w)*dPq(w) + S22(w)*dPj(w)
        dT = lambda w: S11(w)*dPq(w) + S12(w)*dPj(w)

        return dI, dT

    def NEI_to_NEP(self, dI, in_f=False):

        _, S = self.responsivity_matrix()
        _, _, S21, _ = S
        if in_f:
            return lambda f: dI(f)/S21(dpi*f)
        return lambda w: dI(w)/S21(w)

    def NEI_to_absNEP(self, dI, in_f=False):

        _, S = self.responsivity_matrix()
        _, _, S21, _ = S
        if in_f:
            return lambda f: np.absolute(dI(f)/S21(dpi*f))
        return lambda w: np.absolute(dI(w)/S21(w))

    def dP_to_NEP(self, dPj, dPq):

        _, S = self.responsivity_matrix()
        S11, S12, S21, S22 = S
        dP = lambda w: dPq(w) + S22(w)*dPj(w)/S21(w)

        return dP

    def dP_to_NEI(self, dPj, dPq):

        return self.S_response(dPj, dPq)[0]

    ### Noise sources and models ###

    def _readout_noise(self):
        # Readout noise is NOT an input to which the detector responds. It is `downstream'.

        I = self.det.I
        plateau = self.plateau
        pink_knee = self.pink_knee*dpi
        roll_off = self.roll_off*dpi

        pink_coeff  = plateau * pink_knee
        pink_noise = lambda w: pink_coeff / (1e-3 + w)
        low_pass = lambda w: 1. / np.sqrt((1. + (w / roll_off)**2.))
        dI = lambda w: (pink_noise(w) + plateau) * low_pass(w)

        return dI

    def readout_noise_NEI(self):

        return lambda f: self._readout_noise()(dpi*f)

    def readout_noise_NEP(self):

        return lambda f: self.NEI_to_absNEP(self._readout_noise())(dpi*f)

    def _photon_noise(self):

        det = self.det
        Popt = det.Popt
        center_freq = det.center_freq
        bandwidth = det.bandwidth

        shot_noise = (4. * np.pi * hbar * center_freq * Popt)
        p = shot_noise + 2.*(Popt**2. / bandwidth)

        dPj = 0.
        dPq = np.sqrt(p)

        return dPj, dPq

    def photon_noise_NEI(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._photon_noise()
        return lambda f: S21(dpi*f)*dPq + S22(dpi*f)*dPj

    def photon_noise_NEP(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._photon_noise()
        return lambda f: dPq + S22(dpi*f)*dPj/S21(dpi*f)

    def _phonon_noise(self):

        det = self.det

        T = det.T
        G = det.get_G()
        Fp = self.get_Fp()

        dPj = 0.
        dPq = np.sqrt(4. * kb * (T**2.) * G * Fp)

        return dPj, dPq

    def phonon_noise_NEI(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._phonon_noise()
        return lambda f: S21(dpi*f)*dPq + S22(dpi*f)*dPj

    def phonon_noise_NEP(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._phonon_noise()
        return lambda f: dPq + S22(dpi*f)*dPj/S21(dpi*f)

    def _johnson_sh_noise(self):

        det = self.det
        Tb = det.Tb
        Rb = det.Rb
        Rsh = det.Rsh
        R = det.R
        I = det.I

        johnson_sh = np.sqrt(4. * kb * Tb * Rsh)

        dPj = I*johnson_sh
        dPq = 0.

        return dPj, dPq

    def johnson_sh_noise_NEI(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._johnson_sh_noise()
        return lambda f: S21(dpi*f)*dPq + S22(dpi*f)*dPj

    def johnson_sh_noise_NEP(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._johnson_sh_noise()
        return lambda f: dPq + S22(dpi*f)*dPj/S21(dpi*f)

    def _johnson_det_noise(self):

        det = self.det
        R = det.R
        I = det.I
        T = det.T
        beta = det.get_beta()

        johnson = np.sqrt(4. * kb * T * R * (1. + 2.*beta))
        dPj = johnson*I
        dPq =-johnson*I

        return dPj, dPq

    def johnson_det_noise_NEI(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._johnson_det_noise()
        return lambda f: S21(dpi*f)*dPq + S22(dpi*f)*dPj

    def johnson_det_noise_NEP(self):

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._johnson_det_noise()
        return lambda f: dPq + S22(dpi*f)*dPj/S21(dpi*f)

    def _excess_noise(self):

        m = self.m
        if m < 1e-20: return 0., 0.

        dPj_n, dPq_n = self._johnson_det_noise()
        dPj = m*dPj_n
        dPq = m*dPq_n

        return dPj, dPq

    def excess_noise_NEI(self):

        if self.m < 1e-20: return lambda f: 0.

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._excess_noise()
        return lambda f: S21(dpi*f)*dPq + S22(dpi*f)*dPj

    def excess_noise_NEP(self):

        m = self.m
        if self.m < 1e-20: return lambda f: 0.

        if self.S is None: self.responsivity_matrix()
        S11, S12, S21, S22 = self.S
        dPj, dPq = self._excess_noise()
        return lambda f: dPq + S22(dpi*f)*dPj/S21(dpi*f)

    def total_NEP(self, plot=False):

        res = {}
        res["readout_NEP"] = self.readout_noise_NEP()
        res["photon_NEP"] = self.photon_noise_NEP()
        res["phonon_NEP"] = self.phonon_noise_NEP()
        res["johnson_sh_NEP"] = self.johnson_sh_noise_NEP()
        res["johnson_det_NEP"] = self.johnson_det_noise_NEP()
        if self.m >= 1e-20: res["excess_NEP"] = self.excess_noise_NEP()
        res["total_NEP"] = quad_add(*res.values())

        if plot:

            max_f = self.roll_off * 2.
            f_arr = np.logspace(0, np.log10(max_f), 10000)
            for ns in plot_colors.keys():
                if self.m < 1e-20 and "excess" in ns: continue
                k = ns+"_NEP"
                y = 1e18*np.absolute(res[k](f_arr))
                lsty = {}
                lsty["lw"] = 2
                lsty["color"] = plot_colors[ns]
                lsty["label"] = ns
                plt.plot(f_arr, y.copy(), **lsty)
            plt.xscale("log")
            plt.yscale("log")
            plt.xlabel(r"$\nu$ [Hz]")
            plt.ylabel(r"$|\delta P|$ [aW/sqrt(Hz)]")
            plt.xlim((1, max_f))
            plt.ylim(ymin=.1)
            plt.legend(ncol=2, prop={'size': 8})

        return res

    def aliased_NEP(self, plot=False, plot_unaliased=False):

        multi_f = self.multi_f
        max_f = self.max_f
        pes = self.total_NEI(plot=False)
        res = self.total_NEP(plot=plot_unaliased)
        for ns in plot_colors.keys():
            if self.m < 1e-20 and "excess" in ns: continue
            k = ns+"_NEP"
            q = ns+"_NEI"
            res["aliased_"+k] = self.NEI_to_NEP(alias(pes[q], multi_f, max_f), in_f=True)

        if plot:

            max_f = self.roll_off * 2.
            f_arr = np.logspace(0, np.log10(max_f), 10000)
            for ns in plot_colors.keys():
                if self.m < 1e-20 and "excess" in ns: continue
                k = "aliased_"+ns+"_NEP"
                y = 1e18*np.absolute(res[k](f_arr))
                lsty = {}
                lsty["ls"] = ":"
                lsty["lw"] = 2
                lsty["color"] = plot_colors[ns]
                lsty["label"] = "aliased_"+ns
                plt.plot(f_arr, y.copy(), **lsty)
            plt.axvline(self.multi_f/2., color='k', lw=2)
            if not plot_unaliased:
                plt.xscale("log")
                plt.yscale("log")
                plt.xlabel(r"$\nu$ [Hz]")
                plt.ylabel(r"$|\delta P|$ [aW/sqrt(Hz)]")
                plt.xlim((1, max_f))
                plt.ylim(ymin=.1)
            plt.legend(ncol=2, prop={'size': 8})

        return res

    def total_NEI(self, plot=False, legend=True, plt_kw=None):

        res = {}
        res["readout_NEI"] = self.readout_noise_NEI()
        res["photon_NEI"] = self.photon_noise_NEI()
        res["phonon_NEI"] = self.phonon_noise_NEI()
        res["johnson_sh_NEI"] = self.johnson_sh_noise_NEI()
        res["johnson_det_NEI"] = self.johnson_det_noise_NEI()
        if self.m >= 1e-20: res["excess_NEI"] = self.excess_noise_NEI()
        res["total_NEI"] = quad_add(*res.values())

        if plot:

            max_f = self.roll_off * 2.
            f_arr = np.logspace(0, np.log10(max_f), 10000)
            for ns in plot_colors.keys():
                if self.m < 1e-20 and "excess" in ns: continue
                k = ns+"_NEI"
                y = 1e12*np.absolute(res[k](f_arr))
                lsty = {}
                lsty["lw"] = 2
                lsty["color"] = plot_colors[ns]
                if legend: lsty["label"] = ns
                if plt_kw is not None: lsty.update(plt_kw)
                plt.plot(f_arr, y.copy(), **lsty)
            plt.xscale("log")
            plt.yscale("log")
            plt.xlabel(r"$\nu$ [Hz]")
            plt.ylabel(r"$|\delta I|$ [pA/sqrt(Hz)]")
            plt.xlim((1, max_f))
            plt.ylim(ymin=1)
            if legend: plt.legend(ncol=2, prop={'size': 8})

        return res

    def aliased_NEI(self, plot=False, plot_unaliased=False,
                    legend=True, plt_kw=None):

        res = self.total_NEI(plot=plot_unaliased)
        for ns in plot_colors.keys():
            if self.m < 1e-20 and "excess" in ns: continue
            k = ns+"_NEI"
            res["aliased_"+k] = alias(res[k], self.multi_f, self.max_f)

        if plot:

            max_f = self.roll_off * 2.
            f_arr = np.logspace(0, np.log10(max_f), 10000)
            for ns in plot_colors.keys():
                if self.m < 1e-20 and "excess" in ns: continue
                k = "aliased_"+ns+"_NEI"
                y = 1e12*np.absolute(res[k](f_arr))
                lsty = {}
                lsty["ls"] = ":"
                lsty["lw"] = 2
                lsty["color"] = plot_colors[ns]
                if legend: lsty["label"] = "aliased_"+ns
                if plt_kw is not None: lsty.update(plt_kw)
                plt.plot(f_arr, y.copy(), **lsty)
            plt.axvline(self.multi_f/2., color='k', lw=2)
            if not plot_unaliased:
                plt.xscale("log")
                plt.yscale("log")
                plt.xlabel(r"$\nu$ [Hz]")
                plt.ylabel(r"$|\delta I|$ [pA/sqrt(Hz)]")
                plt.xlim((1, max_f))
                plt.ylim(ymin=1)
            if legend: plt.legend(ncol=2, prop={'size': 8})

        return res

    def bar_plot_NEP(self, x=0, y=0, freq=1.5, plot_total=False,
                           exclude_readout=False, label=True):

        res = self.aliased_NEP()
        spc = lambda key: np.absolute(1e12*res[key](freq))**2.

        vals = []
        for ns in plot_colors.keys():
            if self.m < 1e-20 and "excess" in ns: continue
            vals.append(spc("aliased_"+ns+"_NEP"))
        inds = np.argsort(vals)

        curr = y;
        for ns in np.array(plot_colors.keys())[inds]:
            if self.m < 1e-20 and "excess" in ns: continue
            if "total" in ns: continue
            if exclude_readout:
                if "readout" in ns: continue

            a = spc(ns+"_NEP")
            b = spc("aliased_"+ns+"_NEP") - a

            if a > 1e-24:

                plt_kwargs = {"color":plot_colors[ns], "edgecolor":'k'}
                if label: plt_kwargs["label"] = ns
                if plot_total:
                    plt.bar(x+.09, a, width=.78, bottom=curr, **plt_kwargs)
                else: plt.bar(x, a, width=.96, bottom=curr, **plt_kwargs)

                curr += a


            if b > 1e-24:

                plt_kwargs["hatch"] = '//'
                if label: plt_kwargs["label"] = ns + " aliasing"
                if plot_total:
                    plt.bar(x+.09, b, width=.78, bottom=curr, **plt_kwargs)
                else: plt.bar(x, b, width=.96, bottom=curr, **plt_kwargs)

                curr += b

        if plot_total:
            a = spc("total_NEP")
            b = spc("aliased_total_NEP") - a
            plt_kwargs = {"color":plot_colors["total"], "edgecolor":'k'}
            if label: plt_kwargs["label"] = "total"
            plt.bar(x-.39, a, bottom=y, width=.18, **plt_kwargs)
            plt_kwargs["hatch"] = '//'
            if label: plt_kwargs["label"] = "total aliasing"
            plt.bar(x-.39, b, bottom=y+a, width=.18, **plt_kwargs)

        if label: plt.legend()
        plt.ylabel(r"$pA^2/Hz$")

        return spc("total_NEP")

    def bar_plot_NEI(self, x=0, y=0, freq=1.5, plot_total=False,
                           exclude_readout=False, label=True):

        res = self.aliased_NEI()
        spc = lambda key: np.absolute(1e12*res[key](freq))**2.

        vals = []
        for ns in plot_colors.keys():
            if self.m < 1e-20 and "excess" in ns: continue
            vals.append(spc("aliased_"+ns+"_NEI"))
        inds = np.argsort(vals)

        curr = y;
        for ns in np.array(plot_colors.keys())[inds]:
            if self.m < 1e-20 and "excess" in ns: continue
            if "total" in ns: continue
            if exclude_readout:
                if "readout" in ns: continue

            a = spc(ns+"_NEI")
            b = spc("aliased_"+ns+"_NEI") - a

            if a > 1e-24:

                plt_kwargs = {"color":plot_colors[ns], "edgecolor":'k'}
                if label: plt_kwargs["label"] = ns
                if plot_total:
                    plt.bar(x+.09, a, width=.78, bottom=curr, **plt_kwargs)
                else: plt.bar(x, a, width=.96, bottom=curr, **plt_kwargs)

                curr += a

            if b > 1e-24:

                plt_kwargs["hatch"] = '//'
                if label: plt_kwargs["label"] = ns + " aliasing"
                if plot_total:
                    plt.bar(x+.09, b, width=.78, bottom=curr, **plt_kwargs)
                else: plt.bar(x, b, width=.96, bottom=curr, **plt_kwargs)

                curr += b

        if plot_total:
            a = spc("total_NEI")
            b = spc("aliased_total_NEI") - a
            plt_kwargs = {"color":plot_colors["total"], "edgecolor":'k'}
            if label: plt_kwargs["label"] = "total"
            plt.bar(x-.39, a, bottom=y, width=.18, **plt_kwargs)
            plt_kwargs["hatch"] = '//'
            if label: plt_kwargs["label"] = "total aliasing"
            plt.bar(x-.39, b, bottom=y+a, width=.18, **plt_kwargs)

        plt.ylabel(r"$pA^2/Hz$")

        return spc("total_NEI")

    def spec_plots(self):

        plt.subplot(211)
        res = self.aliased_NEI(plot=plot, plot_unaliased=plot)
        plt.xlabel("")

        plt.subplot(212)
        res.update(self.aliased_NEP(plot=plot, plot_unaliased=plot))
        plt.legend('',frameon=False)

    def random_samples(self, spectra=None, N=2**30, fsamp=5e7, debug=False):

        if spectra is None:
            res = self.total_NEI()["total_NEI"]
            spectra = lambda f: res[f]

        fbins = np.fft.fftfreq(N, d=1./fsamp)
        fstep = fbins[1] - fbins[0]
        v = np.zeros(len(fbins))
        pos_f = fbins > 0
        neg_f = fbins < 0
        v[pos_f] = spectra(fbins[pos_f])
        v[neg_f] = np.conjugate(spectra(np.abs(fbins[neg_f])))
        v[np.isnan(v)] = 0
        target_rms = np.sqrt(np.trapz(v[pos_f]**2., x=fbins[pos_f]))
        v*= fstep**.5

        if debug: print "Generating white noise"
        data = np.random.randn(N)
        if debug: print "Taking FFT & applying filter"
        data = np.fft.fft(data)*v
        if debug: print "Taking iFFT"
        data = np.fft.ifft(data)
        if debug: print "Correcting result"
        data = np.real(data)
        data*= target_rms/np.std(data)
        return data

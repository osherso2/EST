#!/bin/bash
source /projects/WCJONES/spider/module_setup.sh
export SPIDER_TOOLS_PATH="/home/bposhers/spider_tools"                          
source $SPIDER_TOOLS_PATH/config/shell/spider_env.bash 

cd /home/bposhers/spider_tools/scratch/bosh/noise_model/EST/projects/S4_detector_design

EST=/home/bposhers/spider_tools/scratch/bosh/noise_model/EST
PYTHONPATH=$PYTHONPATH:$EST/physics_models
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_detector_design
export PYTHONPATH

python compute_dataframe.py $1

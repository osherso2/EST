#!/bin/bash
cd /home/bposhers/spider_tools/scratch/bosh/noise_model/EST/projects/S4_detector_design

a="append"
e="/home/bposhers/spider_tools/scratch/bosh/noise_model/EST/projects/S4_detector_design/logs/compute_err"
o="/home/bposhers/spider_tools/scratch/bosh/noise_model/EST/projects/S4_detector_design/logs/compute_out"
rm $e 
rm $o
args=" -e "$e" -o "$o" --open-mode="$a
for s in 30 40 85 95 145 155 220 270
do
    q="physics"; w="02:30:00"
    n="c"$s
    sbatch $args -t $w -p $q -J $n ./bashCompute.sh $s
done


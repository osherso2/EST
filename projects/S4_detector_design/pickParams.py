import sys
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from parameter_space import * 

df = pd.read_pickle("data/uniframe")                                           
def tolSel(lib, field, val, tol=1e-3):                                          
    sel = np.abs(lib[field] - val)/val < 1e-3                                   
    return sel                                                                  
def tolCut(lib, field, val, tol=1e-3):                                          
    nlib = lib[tolSel(lib, field, val, tol=tol)]                                
    return nlib       
def tolNN(lib, field, val):                                                     
    err = np.array(np.absolute(lib[field] - val))                               
    ind = np.argmin(err)                                                        
    return lib.iloc[ind]  

df = tolCut(df, "r_frac", .6)

title = r"Require $f_{rev}<%.01fkHz$   " % (1e-3*revisit_thresh)                
title+= "good stability   reasonable overdampedness"

fig, axs = plt.subplots(2, 4, sharex=True, sharey=True, figsize=(14, 6))        
fig.suptitle(title)                                                             

def pick(Rn, fig, axs, color='b'):

  lib = tolCut(df, "Rn", Rn)
  C_choices = {}                                                               

  xlims = (.5, 3)                                                             
  ylims = (.2, 3.75)                                                             

  span = {'color':color, 'alpha':.125}

  for j, (c1, c2) in enumerate([(30, 40), (85, 145), (95, 155), (220, 270)]):

    tau1 = tau_m_lims[c1]                                                       
    tau2 = tau_m_lims[c2]                                                       
    nlib1 = tolCut(lib, "config", c1)                                           
    nlib2 = tolCut(lib, "config", c2)                                           
    nlib1 = nlib1[nlib1["tau-"] <= tau1]                                        
    nlib2 = nlib2[nlib2["tau-"] <= tau2]                                        
    C11 = np.nanmin(nlib1["Cx"])                                                
    C12 = np.nanmax(nlib1["Cx"])                                                
    C21 = np.nanmin(nlib2["Cx"])                                                
    C22 = np.nanmax(nlib2["Cx"])                                                
    C1 = max(C11, C21); C2 = min(C12, C22)                                      
    Cc = float(C1 + C2)/2.  

    i1 = j*2                                                                    
    i2 = i1+1                                                                   
    ax1 = axs[i1/4, i1%4]                                                       
    ax2 = axs[i2/4, i2%4]  

    for c in [c1, c2]:                                                          
                                                                                
        if c == c1:                                                             
            ax = ax1                                                            
            nlib = nlib1                                                        
            tau = tau1                                                          
            i = i1                                                              
        else:                                                                   
            ax = ax2                                                            
            nlib = nlib2                                                        
            tau = tau2                                                          
            i = i2      
        C_choices[c] = Cc

        ax.set_title(r"%03dGHz ($\tau_- \leq %.01f ms$)" % (c, 1e3*tau))        
        ax.axhspan(1e12*C1, 1e12*C2, **span)                                    
        if i/4 == 1: ax.set_xlabel(r"L $[\mu H]$")                              
        if i%4 == 0: ax.set_ylabel(r"$C(160mK)$ $[pJ/K]$")                      
                                                                                
        x = 1e06*nlib["L"]                                                      
        y = 1e12*nlib["Cx"]                                                     
        z = 1e-3*nlib["f_rev"]                                                  
                      
        lbl = "__nolegend__" 
        sc = ax.scatter(x, y, s=5, label=lbl, color=color, alpha=.1)
        if i == 7: 
            lbl = r"$R_n=%d m\Omega$" % (1e3*Rn)                      
            sty = {"marker":"o", "s":50, "color":color}
            ax.scatter([np.nan], [np.nan], label=lbl, **sty)
            ax.legend()

        if i/4 == 1: ax.set_xlabel(r"L $[\mu H]$")                              
        if i%4 == 0: ax.set_ylabel(r"$C(160mK)$ $[pJ/K]$") 

  return C_choices

cLib = {}
cLib1 = pick(08e-3, fig, axs, color='b')
cLib2 = pick(12e-3, fig, axs, color='g')
cLib3 = pick(16e-3, fig, axs, color='m')
for i, c in enumerate(configs):
    Cc = (cLib1[c] + cLib2[c] + cLib3[c])/3. 
    Cc = round(2e13*Cc)/2.e13
    cLib[c] = Cc
L_handpicked = {}
L_handpicked[(30, 8e-3)] = 1.75e-6
L_handpicked[(30, 12e-3)] = 2e-6
L_handpicked[(30, 16e-3)] = 2.5e-6
L_handpicked[(40, 8e-3)] = 1e-6 
L_handpicked[(40, 12e-3)] = 1.35e-6
L_handpicked[(40, 16e-3)] = 1.85e-6
L_handpicked[(85, 8e-3)] = 1e-6 
L_handpicked[(85, 12e-3)] = 1.25e-6
L_handpicked[(85, 16e-3)] = 1.75e-6
L_handpicked[(145, 8e-3)] = 0.7e-6
L_handpicked[(145, 12e-3)] = 1e-6
L_handpicked[(145, 16e-3)] = 1.25e-6
L_handpicked[(95, 8e-3)] = 0.95e-6
L_handpicked[(95, 12e-3)] = 1.25e-6
L_handpicked[(95, 16e-3)] = 1.75e-6
L_handpicked[(155, 8e-3)] = 0.7e-6
L_handpicked[(155, 12e-3)] = 1e-6
L_handpicked[(155, 16e-3)] = 1.35e-6
L_handpicked[(220, 8e-3)] = 0.65e-6
L_handpicked[(220, 12e-3)] = 0.95e-6
L_handpicked[(220, 16e-3)] = 1.3e-6
L_handpicked[(270, 8e-3)] = 0.5e-6 
L_handpicked[(270, 12e-3)] = 0.75e-6
L_handpicked[(270, 16e-3)] = 1e-6
paramChoices = {}
for i, c in enumerate([30, 40, 85, 145, 95, 155, 220, 270]):
    ax = axs[i/4, i%4]
    for Rn in [8e-3, 12e-3, 16e-3]:
        paramChoices[(c, Rn)] = (L_handpicked[(c, Rn)], cLib[c])
    ax.scatter(1e6*L_handpicked[(c, 8e-3)], 1e12*cLib[c], color='b', s=10)
    ax.scatter(1e6*L_handpicked[(c, 12e-3)],1e12*cLib[c], color='g', s=10)
    ax.scatter(1e6*L_handpicked[(c, 16e-3)],1e12*cLib[c], color='m', s=10)

plt.show()                                                                      
with open('./data/paramChoices', 'wb') as handle:         
    pickle.dump(paramChoices, handle, protocol=pickle.HIGHEST_PROTOCOL)          

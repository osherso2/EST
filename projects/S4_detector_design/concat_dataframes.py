#!/bin/python 

import numpy as np
import pandas as pd
from parameter_space import configs

nan = np.nan

arr = []
for c in configs:

    print "%03dGHz" % c
    df = pd.read_pickle("data/%03dGHz.pkl" % c)
    arr.append(df)
    
uf = pd.concat(arr)
uf.to_pickle("data/uniframe")

import numpy as np                                                              

configs=[30, 40, 85, 95, 145, 155, 220, 270]

# Threshhold for revisit rate
revisit_thresh = 15.6e3

# R/Rn choices                                                                  
f_arr=[.3, .4, .5, .6, .7, .8]                                                      
                                                                                
# Rn choices                                                                    
Rn_arr=[8e-3, 12e-3, 16e-3]

# L choices                                                                     
delta_L = 1.25e-8
L_arr=np.arange(2e-7, 3e-6+delta_L, delta_L)  

# Won't need to compute tau_arr past:
tau_m_lims = {}                                                                 
tau_m_lims[270] = 1e-3                                                          
tau_m_lims[220] = 1e-3                                                          
tau_m_lims[155] = 1.5e-3                                                        
tau_m_lims[145] = 1.5e-3                                                        
tau_m_lims[95]  = 1.5e-3                                                        
tau_m_lims[85]  = 1.5e-3                                                        
tau_m_lims[40]  = 4e-3                                                        
tau_m_lims[30]  = 4e-3  

# tau choices
tau_arrs = {}
for c in tau_m_lims.keys():
    tau = tau_m_lims[c]
    tau_arrs[c] = np.linspace(.25e-3, tau, 11)


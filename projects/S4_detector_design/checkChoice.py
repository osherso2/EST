import os, sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

lib = pd.read_pickle("./data/unichoice")
def tolSel(lib, field, val, tol=1e-3):                                          
    sel = np.abs(lib[field] - val)/val < 1e-3                                   
    return sel                                                                  
def tolCut(lib, field, val, tol=1e-3):                                          
    nlib = lib[tolSel(lib, field, val, tol=tol)]                                
    N = len(nlib)                                                               
    if N == 1: return nlib.iloc[0]                                              
    return nlib 

rdo = float(sys.argv[1])
lib = tolCut(lib, "SQUID_plateau", rdo)

if rdo < 1e-12: penalty_thresh = 1.01
else: penalty_thresh = 1.05

cmap = plt.cm.nipy_spectral
styles = {}
styles[8] = "s"
styles[12] = "o"
styles[16] = "v"

za = 1.
zb = penalty_thresh 
fig, axs = plt.subplots(2, 4, figsize=(14, 6))
fig.suptitle(r"Checking (L,C) choices for $f_{rev} = 15.6kHz$ SQUID: $(%.1e A/\sqrt{Hz}, 2.5MHz)$" % rdo)
for i, c in enumerate([30, 40, 85, 145, 95, 155, 220, 270]):

    ax = axs[i/4, i%4]
    ax.set_title("%03dGHz" % c)

    if i/4 == 1: ax.set_xlabel(r"L $[\mu H]$")
    if i%4 == 0: ax.set_ylabel(r"$C(T\approx T_c)$ $[pJ/K]$")

    nlib = tolCut(lib, "config", c)
    L_order = []
    for Rn in [8, 12, 16]:
        mlib = tolCut(nlib, "Rn", Rn)
        L_order.append(np.unique(mlib.L)[0])
    Rn_arr = np.array([8, 12, 16])
    Rn_arr = Rn_arr[np.argsort(L_order)]

    for j, Rn in enumerate(Rn_arr):

        mlib = tolCut(nlib, "Rn", Rn)

        errc = 'k' 
        if j == 0: lbl = "aliasing > %.02f" % penalty_thresh
        else: lbl = "__nolegend__"
        olib = mlib[mlib["penalty"] > penalty_thresh]
        x = np.array(olib.L); y = np.array(olib.C)
        ax.scatter(x, y, s=100, c=errc, marker="o", label=lbl)

        if j == 0: lbl = "unstable"
        else: lbl = "__nolegend__"
        olib = mlib[np.logical_not(mlib.stable)]
        x = np.array(olib.L); y = np.array(olib.C)
        ax.scatter(x, y, s=80, c=errc, marker='+', label=lbl)

        if j == 0: lbl = "underdamped"
        else: lbl = "__nolegend__"
        olib = mlib[np.logical_not(mlib.overdamped)]
        x = np.array(olib.L); y = np.array(olib.C)
        ax.scatter(x, y, s=80, c=errc, marker='x', label=lbl)

        style = {"s":30, "marker":styles[Rn], "alpha":1., "label":"__nolegend__"}
        x = np.array(mlib.L); y = np.array(mlib.C)
        sc = ax.scatter(x, y, c=mlib["penalty"], 
                        vmin=za, vmax=zb, cmap=cmap, **style)

        L = np.unique(mlib.L)[0]
        C1 = np.min(mlib.C)
        C2 = np.max(mlib.C)
        ax.set_ylim((.995*C1, 1.005*C2))
        DeltaC = np.mean(np.diff(mlib.C))
        if j == 0: ha = "left"
        if j == 1: ha = "center"
        if j == 2: ha = "right"
        ax.text(L, C2+DeltaC/4., r"$%.1fm\Omega$" % Rn, ha=ha)
        if j == 0:
            if i == 0:
                ax.text(L*1.005, C2-(DeltaC/3.), r"R/Rn=.3")
                ax.text(L*1.005, C1-(DeltaC/3.), r"R/Rn=.8")

clabel = r"aliased_NEI/NEI"
cbaxes = fig.add_axes([.925, 0.19, 0.03, 0.69])
cb = plt.colorbar(sc, cax=cbaxes, label=clabel)
handles, labels = ax.get_legend_handles_labels()
lgd = ax.legend(handles, labels, loc='upper center', bbox_to_anchor=(1.25,0.2))
plt.show()

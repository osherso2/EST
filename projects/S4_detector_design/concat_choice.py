#!/bin/python 

import numpy as np
import pandas as pd
nan = np.nan

arr = []
for c in [30, 40, 85, 95, 145, 155, 220, 270]:

    print "%03dGHz" % c
    df = pd.read_pickle("data/paramChoices_%03dGHz" % c)
    arr.append(df)
    
uf = pd.concat(arr)
uf.to_pickle("data/unichoice")

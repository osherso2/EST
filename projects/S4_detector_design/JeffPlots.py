import sys
import numpy as np
import pandas as pd
import pickle

from load_det import *
min_args = {"method":"SLSQP", "options":{"maxiter":2e4}}

fname = "./data/paramChoices"
with open(fname, 'read') as handle:                                   
    paramChoices = pickle.load(handle) 

c = 220 # GHz 
Rn = 12e-3 # Ohms, normal resistance 
f_rev = 15.6e3 # Hz, revisit rate
SQUID_bw = 1.6e6 # Hz
rdo = 2.5e-12 # SQUID noise plateau A/sqrt(Hz)

tes0_kw = {}
tes0_kw["Rn"] = Rn
det_kw = {}
nm_kw = {}
nm_kw["revisit_rate"] = f_rev 
nm_kw["readout_plateau"] = rdo
nm_kw["roll_off"] = SQUID_bw 

L, Cx = paramChoices[(c, Rn)]

title = r"%03dGHz  " % c
title+= r"$R_n = %.01f m \Omega$" % (1e3*Rn)
title+= "\n"
title+= r"$L=%.1f \mu H$  " % (1e6*L)
title+= r"$C_c=%.2f pJ/K$" % (1e12*Cx)
title+= "\n"

det_kw["L"] = L                                                             
det_kw["Cx"] = Cx 
det = load_det(c, tes0_kw=tes0_kw, det_kw=det_kw)

nm = load_nm(det, nm_kw=nm_kw)
nf = nm.aliased_NEI(plot=True, plot_unaliased=True)
uNEI = nf["total_NEI"](1.)
aNEI = nf["aliased_total_NEI"](1.)

penalty = (aNEI/uNEI)
Lcrit1, Lcrit2 = det.get_Lcrits()
taup, taum, _, _ = det.get_taus()

plt.annotate(r"$\tau_-=%.1f ms$" % (1e3*np.real(taum)), (.6, .15), xycoords='axes fraction')
plt.annotate(r"aliasing penalty: %.03f" % penalty, (.6, .1), xycoords='axes fraction')

plt.suptitle(title)                                             
sname = "%03dGHz_Rn%dmO.pdf" % (c, 1e3*Rn)
plt.savefig("./figures/JeffPlots/"+sname, format='pdf', dpi=800)

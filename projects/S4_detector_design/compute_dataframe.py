#!/usr/bin/python
import sys, os
import numpy as np
import pandas as pd
from itertools import product

from load_det import load_det, load_nm 

from parameter_space import * 

nan = np.nan

magic_num = (1.01**2) - 1

# Pick a configuration
config = int(sys.argv[1])
assert (config in configs)

# an iterable of the above choices
val_prods = product(Rn_arr, tau_arrs[config], L_arr)

min_args = {"method":"SLSQP", "options":{"maxiter":2e4}} 

freqs = np.logspace(3, 5, 2000)

def analyze_det(det, nm, config, tau, Rn, L, f):
    taus = det.get_taus()
    res = {}
    res["config"] = config 
    res["target_tau-"] = tau
    res["Rn"] = Rn 
    res["L"] = L 
    res["Ic"] = det.TESs[0].Ic 
    res["Rn"] = Rn 
    res["L"] = L
    res["stable"] = det.stable() 
    res["overdamped"] = det.overdamped() 
    res["r_frac"] = f 
    res["Vb"] = det.Vb 
    res["R"] = det.R 
    res["I"] = det.T 
    res["T"] = det.T 
    res["Cx"] = det.Cx 
    res["Gx"] = det.Gx
    res["C"] = det.get_C()
    res["G"] = det.get_G() 
    res["betaI"] = det.get_beta() 
    res["alphaI"] = det.get_alpha() 
    res["tau+"] = taus[0] 
    res["tau-"] = taus[1] 

    NEPs = nm.aliased_NEP()
    g = lambda key, fs: np.absolute(NEPs[key](fs))
    res["NEP_det"] = g("aliased_total_NEP", 1.)
    res["NEP_pht"] = g("aliased_photon_NEP", 1.)
    res["NEP_phn"] = g("aliased_phonon_NEP", 1.)

    NEIs = nm.aliased_NEI()
    detNEI = lambda fs: np.absolute(NEIs["total_NEI"](fs))

    # What is the frequency at which aliasing penalty < 1%? 
    # a.k.a. when aliased(detNEI)(1Hz) = m*unaliased_detNEI(freq)
    # EDIT: This is consistently wrong: Probably need sqrt(2)
    # to account for both negative and positive frequencies.
    f_rev = np.nan
    N_rat = 0
    for i in range(1, 21): N_rat += detNEI(i*freqs)**2. 
    N_rat/= detNEI(1.)**2.
    if not np.all(N_rat > magic_num):
        sel = N_rat < magic_num
        sel = np.where(sel)[0][0]
        if sel != 0 and sel != len(N_rat)-1:
            y = freqs[sel-1:sel+1]
            x = N_rat[sel-1:sel+1]
            f_rev = np.sqrt(2.)*np.interp(magic_num, x, y)
    res["f_rev"] = f_rev

    s = det.stable()
    if f > .35 and f < .75:
        s = s and (f_rev <= 15.6e3) and det.overdamped()
    
    return res, s

arr = []
for Rn, tau, L in val_prods:

    if tau > tau_m_lims[config]: continue

    print
    print "%03dGHz" % config,
    print "Rn: %.01f mO" % (1e3*Rn),
    print "tau: %.01f ms" % (1e3*tau),
    print "L: %.01f uH" % (1e6*L)

    det_kw = {}
    det_kw["L"] = L

    tes0_kw = {}
    tes0_kw["Rn"] = Rn

    # Loading det
    det = None
    try:
        det = load_det(config, tes0_kw=tes0_kw, det_kw=det_kw, tau_minus=tau)
        state = det.save_pkl()
    except Exception as e:
        print "failed to generate detector: ", e
        continue
    # Finding bias point
    srr = []
    s = True
    for f in f_arr:

        print ".",
        #sys.stdout.flush()
        if not s: continue

        s = det.find_bias(R_frac=f, min_args=min_args)
        if not s: continue
        nm = load_nm(det, nm_kw={"readout_plateau":1e-18})
        
        res, s = analyze_det(det, nm, config, tau, Rn, L, f)
        if s: srr.append(res.copy())

    if s:
        for r in srr:
            arr.append(r.copy())
    
dataframe = pd.DataFrame(arr)
dataframe.to_pickle("data/%03dGHz.pkl" % config)


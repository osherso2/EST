import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from parameter_space import f_arr, R_arr, tau_arr, L_arr, configs

nan = np.nan

lib = pd.read_pickle("data/uniframe")
lib = lib[lib.keep]

styles = {} 
styles[.3] = "v"
styles[.4] = "x"
styles[.5] = "o"
styles[.6] = "+"
styles[.7] = "s"
styles[.8] = "*"
colors = {}
colors[.3] = "tab:cyan"
colors[.4] = "tab:blue"
colors[.5] = "tab:green"
colors[.6] = "tab:olive"
colors[.7] = "tab:pink"
colors[.8] = "tab:red"

def tolSel(lib, field, val, tol=1e-3):
    sel = np.abs(lib[field] - val)/val < 1e-3
    return sel 
def tolCut(lib, field, val, tol=1e-3):
    nlib = lib[tolSel(lib, field, val, tol=tol)]
    N = len(nlib)
    if N == 1: return nlib.iloc[0], N
    return nlib, N 

fig, axs = plt.subplots(2, 4)
for i, c in enumerate(configs):
    ax = axs[i/4, i%4]
    ax.set_title("%03dGHz" % c)
    ax.set_xlabel(r"target $\tau_- \ [ms]$")
    ax.set_ylabel(r"$\tau_- \ [ms]$")
    ax.set_xlim((1e3*tau_arr[0], 1e3*tau_arr[-1]))
    ax.set_ylim((9e2*tau_arr[0], 1.35e3*tau_arr[-1]))

    nlib, res = tolCut(lib, "config", c)
    assert(res > 0)
    for j, f in enumerate(f_arr):
        offset = (j-2)/200.
        color = colors[f]
        style = styles[f]
        mlib, res = tolCut(nlib, "r_frac", f)
        assert(res > 0)
       
        x = 1e3*np.real(mlib["target_tau-"])
        y = 1e3*np.real(mlib["tau-"])

        ax.plot(x+offset, y, style, color=color, alpha=.5)

    ax.plot(1e3*tau_arr, 1e3*tau_arr, color='k')
    ax.plot(1e3*tau_arr, 1.28*1e3*tau_arr, ":", color='k')

for f in f_arr:
    label = "R/Rn=%.1f" % f 
    style = styles[f]
    color = colors[f]
    plt.plot(-1, -1, style, color=color, label=label)
plt.plot([-1, -2], [-1, -2], color='k', label="y = x")
plt.plot([-1, -2], [-1, -2], ":", color='k', label="y = 1.28*x")
plt.legend(ncol=2, fontsize=8)

fig.suptitle(r"How well did I pick C to get target $\tau_-$?")
fig.tight_layout()
plt.show()

       

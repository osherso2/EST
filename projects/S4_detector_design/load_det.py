# This creates the library of parameter values using information from
# the google sheets made available to me and things Jeff (jpf) tells me.
# Updated 2021-06-08
# bosh
import pickle
import numpy as np
from scipy.optimize import root
from TES_model import *
from DET_model import *
from NSE_model import *

nan = np.nan

# S4 parameters (updated 2021-06-08)

configs = {}

params_30GHz = {}
params_30GHz["band_center"] = 30.0e9 # Hz
params_30GHz["nu_low"] = 25.5e9 # Hz
params_30GHz["nu_high"] = 34.5e9 # Hz
params_30GHz["P_optical"] = 0.52e-12 # W
params_30GHz["safety_factor"] = 2.5
configs[30] = params_30GHz

params_40GHz = {}
params_40GHz["band_center"] = 40.0e9 # Hz
params_40GHz["nu_low"] = 34.0e9 # Hz
params_40GHz["nu_high"] = 46.0e9 # Hz
params_40GHz["P_optical"] = 1.1e-12 # W
params_40GHz["safety_factor"] = 2.5
configs[40] = params_40GHz

params_85GHz = {}
params_85GHz["band_center"] = 85.0e9 # Hz
params_85GHz["nu_low"] = 74.8e9 # Hz
params_85GHz["nu_high"] = 95.2e9 # Hz
params_85GHz["P_optical"] = 2.2e-12 # W
params_85GHz["safety_factor"] = 2.5
configs[85] = params_85GHz

params_95GHz = {}
params_95GHz["band_center"] = 95.0e9 # Hz
params_95GHz["nu_low"] = 83.6e9 # Hz
params_95GHz["nu_high"] = 106.4e9 # Hz
params_95GHz["P_optical"] = 2.0e-12 # W
params_95GHz["safety_factor"] = 2.8
configs[95] = params_95GHz

params_145GHz = {}
params_145GHz["band_center"] = 145.1e9 # Hz
params_145GHz["nu_low"] = 129.1e9 # Hz
params_145GHz["nu_high"] = 161.0e9 # Hz
params_145GHz["P_optical"] = 3.1e-12 # W
params_145GHz["safety_factor"] = 2.7
configs[145] = params_145GHz

params_155GHz = {}
params_155GHz["band_center"] = 155.1e9 # Hz
params_155GHz["nu_low"] = 138.0e9 # Hz
params_155GHz["nu_high"] = 172.1e9 # Hz
params_155GHz["P_optical"] = 3.3e-12 # W
params_155GHz["safety_factor"] = 2.5
configs[155] = params_155GHz

params_220GHz = {}
params_220GHz["band_center"] = 220.0e9 # Hz
params_220GHz["nu_low"] = 195.8e9 # Hz
params_220GHz["nu_high"] = 244.2e9 # Hz
params_220GHz["P_optical"] = 6.3e-12 # W
params_220GHz["safety_factor"] = 2.5
configs[220] = params_220GHz

params_270GHz = {}
params_270GHz["band_center"] = 270.0e9 # Hz
params_270GHz["nu_low"] = 240.3e9 # Hz
params_270GHz["nu_high"] = 299.7e9 # Hz
params_270GHz["P_optical"] = 8.7e-12 # W
params_270GHz["safety_factor"] = 2.5
configs[270] = params_270GHz


# HELPER FUNCTIONS TO TRANSLATE GROUP'S CHOSEN PARAMETERS TO MY MODEL

def get_Ic(PJ, Rn=16e-3, betaI=.5, alphaI=75.):
    
    factor1 = (alphaI/(3.*betaI))**(1.5)
    factor2 = (PJ/Rn)**.5
    
    return 4.*factor1*factor2

def get_Gx(Popt, safety_factor, betaG=2.5):                         
    
    Tc = .16
    Tb = .10

    P = Popt*safety_factor
    n = betaG + 1                                                               
    Gc = P*(Tc**betaG)*n/(Tc**n - Tb**n)                                        
    return Gc  

def get_Cx(L, R, G, PJ, Rsh=4e-4, tau_minus=1e-3):

    T = .16 # [K]
    betaI = .5
    alphaI = 75

    LG = PJ*alphaI / (G*T)
    tau_LR = L/R                                                            
    tau_el = tau_LR / (1 + betaI + Rsh/R)

    def err(C):
    
        tau_CG = C/G                                                            
        tau_tm = tau_CG / (1. - LG)                                      
                                                                                
        disc = ((tau_tm - tau_el)/(tau_tm * tau_el))**2.                        
        disc-= 4.* LG * (2. + betaI) / (tau_LR * tau_CG)                  
        disc = complex(disc)**.5                                                
        term = (tau_tm + tau_el)/(tau_tm * tau_el)                              
        return np.absolute(2/(term - disc) - tau_minus)
        
    res = root(err, x0=1e-12)
    if res.success: return res.x*.9525
    else: return nan 

TES0 = {}
TES0["name"] = "sci" # name, here specifying science transition
TES0["Ic"] = .01 # [A] crit current
TES0["Tc"] = .16 # [K] crit temp
TES0["Rn"] = 16e-3 # [Ohms] ~PLACEHOLDER
TES0["alpha0"] = 75 # ~PLACEHOLDER (dln(R)/dln(T) | T=Tc,I=0
# The following values will change during code execution (placeholding)         
TES0["I"] = 0.0 # [A] current through TES
TES0["T"] = 0.1 # [K] temperature of TES
    
TES1 = {}
TES1["name"] = "lab" # name, here specifying lab transition
TES1["Ic"] = .01 # [A] crit current
TES1["Tc"] = .45 # [K] crit temp 
TES1["Rn"] = 32e-3 # [Ohms] # ~PLACEHOLDER normal res
TES1["alpha0"] = 37.5 # PLACEHOLDER (dln(R)/dln(T) | T=Tc,I=0
# The following values will change during code execution (placeholding)         
TES1["I"] = 0.0 # [A] current through TES
TES1["T"] = 0.1 # [K] temperature of TES
    
DET = {}                                                                 
DET["Tx"] = .16 # Reference temp for C, G [K]                            
DET["Cx"] = 1.0e-12 # PLACEHOLDER Heat capacity at Tx [J/K]                              
DET["Gx"] = 1.0e-10 # Therm conductance at Tx [W/K]                          
DET["betaC"] = 1.0 # Exponent on heat capacity                           
DET["betaG"] = 2.5 # PLACEHOLDER Exponent on therm conductivity                      
DET["num_rows"] = 64 # Number of equiv dets per bias line                
DET["L"] = .2e-6 # Inductance facing det [H]                               
DET["Rsh"] = 4e-04 # Shunt resistance [Ohms]                             
DET["Rb"] = 1.0e+03 # Bias resistor [Ohms]                               
DET["Rl"] = 0.0 # Parasitic res on island [Ohms]                         
DET["Rs"] = 0.0 # Parasitic res off island [Ohms]                        
DET["Popt"] = .5e-12 # Optical power on island [W]                          
DET["center_freq"] = 100e9 # Center freq of observing band [Hz]            
DET["bandwidth"] = 50e9 # Width of observing band [Hz]                    
# The following values will change during code execution (placeholding)         
DET["Vb"] = 0.0 # Bias voltage [V]                                       
DET["Tb"] = 0.1 # Substrate temp [K]                                     
DET["R"] = 0.0 # Total island res [Ohms]                                 
DET["I"] = 0.0 # [A] current through island 
DET["T"] = 0.0 # [K] temperatures of TES

# Extra noise model parameters
noise_params = {}                                                               
noise_params["det"] = None
noise_params["readout_plateau"] = 2.5e-12 # Noise plateau NEI [A/sqrt(Hz)]
noise_params["pink_knee"] = 2. # [Hz]
noise_params["roll_off"] = 2.5e6 # [Hz]
noise_params["m"] = 2. # Excess noise parameter
noise_params["revisit_rate"] = 1.5e4 # [Hz]

def load_det(config, tes0_kw=None, tes1_kw=None, det_kw=None, 
             betaI=.5, tau_minus=1e-3):
    """
    Input
    ------
    name: either a string like "SAT_030GHz_sci" or an int like 30

    Output
    ------
    det, nm: A detector model and a noise model

    Optional input
    ------
    tes0_kw: a dictionary of parameters to overwrite in TES0
    tes1_kw: a dictionary of parameters to overwrite in TES1
    det_kw:  a dictionary of parameters to overwrite in DET 
    nm_kw:   a dictionary of parameters to overwrite in NOISE_MODEL 
    """

    params = configs[config]
    Popt = params["P_optical"]                                              
    sf = params["safety_factor"]  
    PJ = Popt * (sf - 1.)

    TES0_mod = TES0.copy()
    if tes0_kw is not None:
        for k in tes0_kw.keys(): TES0_mod[k] = tes0_kw[k]
    TES1_mod = TES1.copy()
    if tes1_kw is not None:
        for k in tes1_kw.keys(): TES1_mod[k] = tes1_kw[k]

    DET_mod = DET.copy()
    DET_mod["Popt"] = Popt
    DET_mod["center_freq"] = params["band_center"]
    DET_mod["bandwidth"] = params["nu_high"] - params["nu_low"]
    if det_kw is not None:
        for k in det_kw.keys(): DET_mod[k] = det_kw[k]

    Ic = get_Ic(PJ, Rn=TES0_mod["Rn"], betaI=betaI, alphaI=TES0_mod["alpha0"])
    Gx = get_Gx(Popt, sf, betaG=DET_mod["betaG"])
    Cx = get_Cx(DET_mod["L"], TES0_mod["Rn"]/2., Gx, PJ, tau_minus=tau_minus)

    if tes0_kw is None:
        TES0_mod["Ic"] = Ic
    elif "Ic" not in tes0_kw.keys():
        TES0_mod["Ic"] = Ic
    tes0 = TES(**TES0_mod) 
    if tes1_kw is None:
        TES1_mod["Ic"] = 2.*TES0_mod["Ic"]
        TES1_mod["Rn"] = 2.*TES0_mod["Rn"]
        TES1_mod["alpha0"] = TES0_mod["alpha0"]/2.
    else:
        if "Ic" not in tes1_kw.keys():
            TES1_mod["Ic"] = 2.*TES0_mod["Ic"]
        if "Rn" not in tes1_kw.keys():
            TES1_mod["Rn"] = 2.*TES0_mod["Rn"]
        if "alpha0" not in tes1_kw.keys():
            TES1_mod["alpha0"] = TES0_mod["alpha0"]/2.
    tes1 = TES(**TES1_mod) 
    
    if det_kw is None:
        DET_mod["Gx"] = Gx
    else:
        if "Gx" not in det_kw.keys():
            DET_mod["Gx"] = Gx
        if "Cx" not in det_kw.keys(): 
            DET_mod["Cx"] = Cx

    DET_mod["TESs"] = [tes0, tes1]
    det = detector(**DET_mod)
    det.halfway_finder()
   
    assert(not np.isnan(det.R))
    assert(not np.isnan(det.Cx))
    return det

def load_nm(det, nm_kw=None):

    noise_params["det"] = det
    if nm_kw is not None:
        for k in nm_kw.keys(): noise_params[k] = nm_kw[k]
    nm = noise(**noise_params)

    return nm

import sys
import numpy as np
import pandas as pd
import pickle

from load_det import *
from parameter_space import configs 
min_args = {"method":"SLSQP", "options":{"maxiter":2e4}}

nan = np.nan

results = [] 
result = {}                                                                     
result["config"] = nan                                                               
result["SQUID_plateau"] = nan                                                               
result["SQUID_bw"] = nan                                                               
result["revisit_rate"] = nan                                                               
result["Rn"] = nan                                                               
result["r_frac"] = nan                                                          
result["L"] = nan                                                               
result["C"] = nan                                                               
result["Cx"] = nan                                                               
result["G"] = nan                                                               
result["alphaI"] = nan                                                          
result["betaI"] = nan                                                           
result["tau+"] = nan                                                            
result["tau-"] = nan                                                            
result["stable"] = nan                                                          
result["overdamped"] = nan                                                      
result["loop_gain"] = nan                                                       
result["Lcrits"] = (nan, nan)                                                   
result["Lthresh"] = nan   
result["readout_NEI"] = nan                                                     
result["photon_NEI"] = nan                                                      
result["phonon_NEI"] = nan                                                      
result["johnson_sh_NEI"] = nan                                                  
result["johnson_det_NEI"] = nan                                                 
result["excess_NEI"] = nan                                                      
result["total_NEI"] = nan                                                       
result["aliased_readout_NEI"] = nan                                             
result["aliased_photon_NEI"] = nan                                              
result["aliased_phonon_NEI"] = nan                                              
result["aliased_johnson_sh_NEI"] = nan                                          
result["aliased_johnson_det_NEI"] = nan                                         
result["aliased_excess_NEI"] = nan                                              
result["aliased_total_NEI"] = nan                                               
result["penalty"] = nan     
result["config_units"] = ["GHz"] 
result["SQUID_plateau_units"] = ["A/sqrt(Hz)"]
result["SQUID_bw_units"] = ["Hz"]
result["revisit_rate_units"] = ["Hz"] 
result["Rn_units"] = ["mOhms"]
result["r_frac_units"] = [""]
result["L_units"] = ["uH"]
result["C_units"] = ["pJ/K"]
result["Cx_units"] = ["pJ/K"]
result["G_units"] = ["pW/K"]
result["alphaI_units"] = [""]
result["betaI_units"] = [""] 
result["tau+_units"] = ["us"]
result["tau-_units"] = ["ms"]
result["stable_units"] = [""]
result["overdamped_units"] = [""]
result["loop_gain"] = [""] 
result["Lcrits_units"] = ["uH", "uH"] 
result["Lthresh_units"] = ["uH"] 
result["readout_NEI_units"] = ["pA/sqrt(Hz)"]
result["photon_NEI_units"] = ["pA/sqrt(Hz)"]
result["phonon_NEI_units"] = ["pA/sqrt(Hz)"]
result["johnson_sh_NEI_units"] = ["pA/sqrt(Hz)"]
result["johnson_det_NEI_units"] = ["pA/sqrt(Hz)"]
result["excess_NEI_units"] = ["pA/sqrt(Hz)"]
result["total_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_readout_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_photon_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_phonon_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_johnson_sh_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_johnson_det_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_excess_NEI_units"] = ["pA/sqrt(Hz)"]
result["aliased_total_NEI_units"] = ["pA/sqrt(Hz)"]
result["penalty_units"] = [""] 

Rns = [8e-3, 12e-3, 16e-3]
c = int(sys.argv[1])

fname = "./data/paramChoices"
with open(fname, 'read') as handle:                                   
    paramChoices = pickle.load(handle) 

f_rev = 15.6e3
SQUID_bw = 1.6e6
print "\n\n"
for rdo in [1e-18, 2.5e-12]:
    print "SQUID NEI plateau: %.1e A/sqrt(Hz)" % rdo
    for Rn in Rns:

        print "  Rn: %d mOhms" % (1e3*Rn) 

        tes0_kw = {}
        tes0_kw["Rn"] = Rn
        det_kw = {}
        nm_kw = {}
        nm_kw["revisit_rate"] = f_rev 
        nm_kw["readout_plateau"] = rdo
        nm_kw["roll_off"] = SQUID_bw 

        if rdo < 1e-12: rdo_str = "noSQ"
        if rdo > 1e-12: rdo_str = "wSQ"

        if c in configs:

            print "    config: %03dGHz" % (c) 
            sname = "%03dGHz_Rn%dmO_%s" % (c, 1e3*Rn, rdo_str)

            L, Cx = paramChoices[(c, Rn)]

            title = r"%03dGHz  " % c
            title+= r"$R_n = %.01f m \Omega$ " % (1e3*Rn)
            title+= r"$SQUID_{NEI} = %.1e A/\sqrt{Hz}$ " % rdo
            title+= "\n"
            title+= r"$L=%.1f \mu H$ " % (1e6*L)
            title+= r"$C_c=%.2f pJ/K$ " % (1e12*Cx)
            title+= r"$\tau_-=%.1f ms$ "
            title+= "\n"

            det_kw["L"] = L                                                             
            det_kw["Cx"] = Cx 
            det = load_det(c, tes0_kw=tes0_kw, det_kw=det_kw)
            title = title % (1e3*np.real(det.get_taus()[1]))

            fig, axs = plt.subplots(3, 1, figsize=(30, 10))
            for i, f in enumerate([.5, .3, .7, .4, .6, .8]):

                print "      R/Rn: %.02f" % f 
                if i < 3:
                    ax = axs[i]  
                    ax.set_title(r"R/Rn = %.01f" % f)
                    plt.sca(ax)

                res = result.copy()
                res["config"] = c
                res["SQUID_plateau"] = rdo
                res["SQUID_bw"] = SQUID_bw 
                res["revisit_rate"] = f_rev 
                res["Rn"] = Rn*1e3

                if f != .5: s = det.find_bias(R_frac=f, min_args=min_args)
                nm = load_nm(det, nm_kw=nm_kw)
                if i < 3:
                    nf = nm.aliased_NEI(plot=True, plot_unaliased=True)
                else: nf = nm.aliased_NEI(plot=False, plot_unaliased=False)
                uNEI = nf["total_NEI"](1.)
                aNEI = nf["aliased_total_NEI"](1.)

                penalty = (aNEI/uNEI)
                Lcrit1, Lcrit2 = det.get_Lcrits()
                taup, taum, _, _ = det.get_taus()

                res["r_frac"] = (det.R/det.TESs[0].Rn)
                res["L"] = (1e6*det.L)
                res["C"] = (1e12*det.get_C())
                res["Cx"] = (1e12*det.Cx)
                res["G"] = (1e12*det.get_G())
                res["alphaI"] = det.get_alpha()
                res["betaI"] = det.get_beta()
                res["tau+"] = (1e6*np.real(taup), 1e6*np.imag(taup))
                res["tau-"] = (1e3*np.real(taum), 1e3*np.imag(taum))
                res["stable"] = det.stable()
                res["overdamped"] = det.overdamped()
                res["loop_gain"] = det.get_loop_gain()
                res["Lcrits"] = (1e6*Lcrit1, 1e6*Lcrit2)
                res["Lthresh"] = (1e6*det.get_Lthresh())
                for k in nf.keys():
                    n = 1e12*np.absolute(nf[k](1.))
                    res[k] = n
                res["penalty"] = res["aliased_total_NEI"]/res["total_NEI"]
                results.append(res.copy())

                if i != 2 and i < 3: plt.xlabel("")
                if f == .5: 
                    title += r"aliasing penalty: %.03f" % penalty 

        fig.suptitle(title, y=1.0025)                                             
        plt.savefig("./figures/paramChoices/"+sname, figsize=(30, 12), dpi=400)
        #plt.show()    

results = pd.DataFrame(results)
results.to_pickle("./data/paramChoices_%03dGHz" % c)

from revX150 import *
import matplotlib.pyplot as plt
import numpy as np

dt = 2.5e-7
t_arr = np.arange(0, .5, dt)
l = len(t_arr)
Vb = np.zeros(l)*det.Vb + .005
Vb[int(l/4):] -=  .005
Vb[int(2*l/4):] +=  .005
Vb[int(3*l/4):] -=  .005
Vb = np.roll(Vb, l/8)

wdir = "/home/bposhers/spider_tools/scratch/"
wdir+= "bosh/noise_model/EST/projects/SPIDER_flux_slips/"
b_arr = np.arange(.06, .16, .01)[::-1]

if False:

   det.halfway_finder()
   state = det.save_pkl()
   reset = lambda: det.load_pkl(state=state)

   rI = det.I
   rT = det.T

   arr_f = []

   sc = False
   res_arr = np.array([])
   det.find_equil(b_arr[0])
   for i, b in enumerate(b_arr):
       s, _ ,_ = det.find_equil(b)
       if not s:
           print "find_equil failed"
           reset()
           det.Vb = b
           det.settle()
       det.settle()
       f = abs(det.R/det.TESs[0].Rn)
       f = int(100*f)/100.
       if f in arr_f:
          continue
          print i, b, det.Vb, "duplicate"
       arr_f.append(f)
       if f < .1:
           if sc:
               print i, b, det.Vb, "fail"
               continue
           sc = True
       print i, b, det.Vb, f
       t, _, _, I, _ = det.Vb_variation(t_arr, Vb+b)
       I = I*1e6
       e = np.append([b, det.Vb, f], I)
       if len(res_arr) == 0: res_arr = e
       else: res_arr = np.vstack((res_arr, e))
       np.save(wdir+"bias_steps", res_arr)

if True:

    res_arr = np.load(wdir+"bias_steps.npy")
    c = lambda i: plt.cm.inferno((i + .5)/(1.+len(res_arr)))



    for i, e in enumerate(res_arr[::-1]):

       b = e[0]
       det.Vb = e[1]
       f = e[2]
       I = e[3:]

       t_arr = np.arange(len(I))*dt
       resamp = int((1./dt)/119)
       plt.plot(t_arr, I, lw=2, c=c(i), label=r"$R/R_n = %0.02f$" % f)
       plt.plot(t_arr[::resamp], I[::resamp], 'o', ms=3, c=c(i))

    plt.xlim((0, max(t_arr)))
    #plt.ylim((-3, 5))
    plt.xlabel("time [s]")
    plt.ylabel(r"$\Delta I \ [\mu A]$")
    plt.legend()
    plt.show()

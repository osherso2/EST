import sys, os

from MCE_model import *

FB = {}
FB['fb_bits'] = 14
FB['fb_maxV'] = .965
FB['fb_R'] = 15040 + 143
FB['m'] = 8

B = {}
B['bias_bits'] = 16
B['bias_maxV'] = 2.5
B['bias_R'] = 953 + 143

from example_tuning import transfer, invtrans

mce_params = {}
mce_params['row_len'] = 100
mce_params['data_rate'] = 1
mce_params['num_rows'] = 33 
mce_params['data_mode'] = 10
mce_params['filter_coeff'] = [32295, 15915, 32568, 16188, 5, 12]
mce_params['closed_loop'] = True
mce_params['transfer'] = transfer
mce_params['invtrans'] = invtrans
mce_params['feedback_params'] = FB
mce_params['bias_params'] = B
mce_params['P'] = 0
mce_params['I'] = 150
mce_params['D'] = 0

mce = MCE(**mce_params)

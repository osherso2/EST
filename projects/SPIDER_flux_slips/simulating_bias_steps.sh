#!/bin/bash

source /projects/WCJONES/spider/module_setup.sh
export SPIDER_TOOLS_PATH="/home/bposhers/spider_tools"
source $SPIDER_TOOLS_PATH/config/shell/spider_env.bash

cd /home/bposhers/spider_tools/scratch/bosh/noise_model/EST/projects/SPIDER_flux_slips/
python simulating_bias_steps.py

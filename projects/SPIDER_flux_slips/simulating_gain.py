from revX150 import *
import matplotlib.pyplot as plt
import numpy as np

det.Popt = 3.5e-13
det.halfway_finder()

plt.rc('font', size=10) #controls default text size
plt.rc('axes', titlesize=10) #fontsize of the x and y titles
plt.rc('axes', labelsize=10) #fontsize of the x and y labels
plt.rc('xtick', labelsize=10) #fontsize of the x tick labels
plt.rc('ytick', labelsize=10) #fontsize of the y tick labels
plt.rc('legend', fontsize=10) #fontsize of the legend
plt.rc('figure', titlesize=10) #fontsize of figure title
plt.rcParams.update({'font.size': 10})

wdir = "/home/bposhers/spider_tools/scratch/"
wdir+= "bosh/noise_model/EST/projects/SPIDER_flux_slips/"

b_arr, R, I, T, F = np.load("loadcurve.npy")
Vbs = np.unique(b_arr)
N = len(b_arr)/len(Vbs)
rb = b_arr[N-3::N]
rI = I[N-3::N]
rT = T[N-3::N]
rR = R[N-3::N]

state = det.save_pkl()
reset = lambda: det.load_pkl(state=state)

if True:

    res_arr = []
    for b, r, i, t in zip(rb, rI, rT, rR):

        reset()
        det.Vb = b
        for i in range(4):
            det.R = r
            det.I = i
            det.T = t
            det.TESs[0].R = r
            det.TESs[0].I = i
            det.TESs[0].T = t
            det.TESs[1].R = 0.0
            det.TESs[1].I = i
            det.TESs[1].T = t
            det.sim([0, 1e-18, 2e-18])
        s = det.settle(Vb=b)
        if( not s) or abs(det.P_bal()) > 1e-9:
            det.Vb = b
            det.R = 2e-5
            det.T = .35
            det.TESs[0].R = 2e-5
            det.TESs[0].T = .35
            det.TESs[1].R = 0.0
            det.TESs[1].T = .35
            det.I = det.get_Ib()
            det.TESs[0].I = det.get_Ib()
            det.TESs[1].I = det.get_Ib()
            for i in range(5):det.sim(np.arange(2e4)*10**(-16 + i))
        if abs(det.P_bal()) > 1e-16:
            print b, "failed"
            continue
        det.sim([0, 1e-16, 2e-16])
        f0 = det.R/det.TESs[0].Rn
        I0 = det.I
        R0 = det.R
        P0 = R0*I0*I0
        det.Popt -=  1e-14
        det.settle()
        det.sim([0, 1e-16, 2e-16])
        f1 = det.R/det.TESs[0].Rn
        I1 = det.I
        R1 = det.R
        P1 = R1*I1*I1
        dP = (P1 - P0)/1e-14
        print b, f0, f1, dP
        res_arr.append([det.Vb, f0, dP])

    res_arr = np.array(res_arr).T
    np.save("gains", res_arr)


if False:

    res = np.load("gains.npy")

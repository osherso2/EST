from revX150 import *
import matplotlib.pyplot as plt
import numpy as np

plt.rc('font', size=6) #controls default text size
plt.rc('axes', titlesize=6) #fontsize of the x and y titles
plt.rc('axes', labelsize=6) #fontsize of the x and y labels
plt.rc('xtick', labelsize=5) #fontsize of the x tick labels
plt.rc('ytick', labelsize=5) #fontsize of the y tick labels
plt.rc('legend', fontsize=6) #fontsize of the legend
plt.rc('figure', titlesize=6) #fontsize of figure title
#plt.rcParams.update({'font.size': 10})

wdir = "/home/bposhers/spider_tools/scratch/"
wdir+= "bosh/noise_model/EST/projects/SPIDER_flux_slips/"

def loadcurve(Vmax=.2, Vmin=.05, DVb=.005, Dt=.1, dt=5e-7, savename=None):

    def get_fbs(det):
        L = det.get_loop_gain()
        b = det.get_beta()
        return 1./(1. + (L/(1. - L))*(2. + b) + b)

    b_arr = np.arange(Vmax, Vmin-DVb, -DVb)
    b_arr = np.repeat(b_arr, int(Dt/dt))
    t_arr = np.arange(len(b_arr))*dt

    det.I = det.get_I(R=det.TESs[0].Rn, Vb=Vmax)
    det.T = det.TESs[0].Tc
    det.Vb = Vmax
    det.find_equil(Vmax)
    det.settle()
    det.settle()

    t, _, R, I, T, F = det.Vb_variation(t_arr, b_arr, custom=get_fbs)

    if not savename is None:
        np.save(wdir+savename, [b_arr, R, I, T, F])

    return t, R, I, T, F


#loadcurve(savename="loadcurve.npy")

b_arr, R, I, T, F = np.load("loadcurve.npy")
b2, _, G2 = np.load("gains.npy")
b2 = b2[::-1]
G2 = G2[::-1]
b_arr = b_arr[::-1]
R = R[::-1]
I = I[::-1]
T = T[::-1]
F = F[::-1]
P = R*I*I

Vbs = np.unique(b_arr)
N = len(b_arr)/len(Vbs)

t_arr = np.arange(len(b_arr))*(.1/N)
t_arr = t_arr[::-1]
mx = max(b_arr); mn = min(b_arr)
c_arr = plt.cm.viridis((b_arr-mn)/(mx-mn))
rb = b_arr[N-3::N]
rt = t_arr[N-3::N]
rc = c_arr[N-3::N]
rI = I[N-3::N]
rT = T[N-3::N]
rR = R[N-3::N]
rP = P[N-3::N]
rF = F[N-3::N]
rG = np.copy(rF)
for i in np.arange(len(rb)):
    b = rb[i]
    j = np.argmin(abs(b2 - b))
    if b not in b2: rG[i] = np.nan
    else: rG[i] = G2[j]


dIb = np.diff(rb / det.Vb)
dIs = np.diff(rI)
rD = ((dIb/dIs) - 1.)*det.Rsh
rD = np.append(rD, rD[-1])

fig, axs = plt.subplots(3, 2)
plt.tight_layout()

plt.sca(axs[0,0])
plt.scatter(t_arr, 1e6*I, color=c_arr, s=1)
plt.scatter(rt, 1e6*rI, color=rc, edgecolor='k', s=7)
plt.xlabel(r"time [s]", labelpad=-.35)
plt.ylabel(r"I [$\mu$ A]", labelpad=-.35)

plt.sca(axs[1,0])
plt.scatter(t_arr, 1e12*P, color=c_arr, s=1)
plt.scatter(rt, 1e12*rP, color=rc, edgecolor='k', s=7)
plt.xlabel(r"time [s]", labelpad=-.35)
plt.ylabel(r"P [nW]", labelpad=-.35)

plt.sca(axs[0,1])
plt.scatter(t_arr, 1e3*R, color=c_arr, s=1)
plt.scatter(rt, 1e3*rR, color=rc, edgecolor='k', s=7)
plt.xlabel(r"time [s]", labelpad=-.35)
plt.ylabel(r"R [$m\Omega$]", labelpad=-.35)

plt.sca(axs[1,1])
plt.scatter(1e3*R, F, color=c_arr, s=1)
plt.scatter(1e3*rR, rF, color=rc, edgecolor='k', s=7)
plt.xlabel(r"R [$m\Omega$]", labelpad=-.35)
plt.ylabel(r"$f_{bs}$", labelpad=-4.05)
plt.xlim((0, 32))
plt.ylim((-1, 1.5))

plt.sca(axs[2,0])
plt.scatter(1e6*I, 1e3*T, color=c_arr, s=1)
plt.scatter(1e6*rI, 1e3*rT, color=rc, edgecolor='k', s=7)
plt.xlabel(r"I [$\mu A$]", labelpad=-.35)
plt.ylabel(r"T [mK]", labelpad=-.5)

plt.sca(axs[2,1])
plt.scatter(1e3*rR, rG, color=rc, edgecolor='k', s=7)
plt.xlabel(r"R [$m\Omega$]", labelpad=-.35)
plt.ylabel(r"$\delta P_{joule}/ \delta P_{opt}$", labelpad=-.35)
plt.xlim((0, 32))

plt.suptitle(r"Loadcurve starting at $V_b=200mV$, stepping $5mV$ every $100ms$", y=1.0015)

plt.savefig("loadcurves", figsize=(12, 12), dpi=400)

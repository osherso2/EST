import sys, os

from TES_model import *
from DET_model import *

# Al TES
Al_params = {}
Al_params['name'] = 'Al'
Al_params['Ic'] = .02
Al_params['Tc'] = 1.5
Al_params['Rn'] = 32e-3
Al_params['alpha0'] = 200
Al_params['I'] = 1.113757407119036e-05
Al_params['T'] = 0.4936060056461061 
Al = TES(**Al_params)

#Ti TES
Ti_params = {}
Ti_params['name'] = 'Ti'
Ti_params["Ic"] = .01
Ti_params["Tc"] = .5
Ti_params['Rn'] = 32e-3
Ti_params['alpha0'] = 125
Ti_params['I'] = 1.113757407119036e-05
Ti_params['T'] = 0.4936060056461061
Ti = TES(**Ti_params)

TESs = [Ti, Al]

det_params = {}
det_params['TESs'] = TESs
det_params['Cx'] = 1e-12
det_params['Gx'] = 20e-12
det_params['Tx'] = .450 
det_params['betaC'] = 2.8
det_params['betaG'] = 2.1
det_params['num_rows'] = 33
det_params['L'] = 2.4e-6
det_params['Rsh'] = 3e-3
det_params['Rb'] = 1096
det_params['Rl'] = 1e-6
det_params['Rs'] = 1e-6
det_params['Popt'] = 3e-13
det_params['center_freq'] = 150e9
det_params['bandwidth'] = 33e9
det_params['Vb'] = 0.07731568655338109
det_params['Tb'] = 0.300
det_params['R'] = 0.016000000000363154
det_params['I'] = 1.113757407119036e-05
det_params['T'] = 0.4936060056461061

det = detector(**det_params)

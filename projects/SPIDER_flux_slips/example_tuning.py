import numpy

sqdata_x, sqdata_y = numpy.load("./example_tuning.npy")
def transfer(q):
    x = sqdata_x
    y = sqdata_y
    q = numpy.array(q)
    n = lambda z: numpy.interp(z, x, y)
    fb0 = min(x)
    fb2 = max(x)
    period = fb2 - fb0
    q = (q - fb0) % period + fb0
    return n(q)

def invtrans(q):
    x = sqdata_x
    y = sqdata_y
    q = numpy.array(q)
    m = lambda z: numpy.interp(z, y, x)
    q[q > max(y)] = max(y)
    q[q < min(y)] = min(y)
    return m(q)



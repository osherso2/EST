from revX150 import det
from mce_example import mce
import matplotlib.pyplot as plt

# Simulate a cosmic ray 
det.find_bias(R_frac=.5, TES_index=0)

plt.suptitle("Depositing 1MeV")
plt.subplot(211)
print "This will take a minute (1/2)"
time, R, I, T = det.cray(E=1e6)
plt.plot(time, 1e9*(I-I[0]), label="underlying signal [nA]")
mce.data_mode = 1
t, d, e = mce.apply_data_mode(time, I)
plt.plot(t, d-d[0], label="mce ADU before filtering")
mce.data_mode = 10
t, d, e = mce.apply_data_mode(time, I)
plt.plot(t, d-d[0], label="mce ADU after filtering")
plt.title("locked at R/Rn = .5")
plt.xlabel("time")

det.find_bias(R_frac=.7, TES_index=0)

plt.subplot(212)
print "This will take a minute (2/2)"
time, R, I, T = det.cray(E=1e6)
plt.plot(time, 1e9*(I-I[0]), label="underlying signal [nA]")
mce.data_mode = 1
t, d, e = mce.apply_data_mode(time, I)
plt.plot(t, d-d[0], label="mce ADU before filtering")
mce.data_mode = 10
t, d, e = mce.apply_data_mode(time, I)
plt.plot(t, d-d[0], label="mce ADU after filtering")
plt.title("locked at R/Rn = .7")
plt.xlabel("time")
plt.legend()

plt.show()

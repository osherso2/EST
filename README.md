EST: Est Simulates TESs 
   --- by bosh


physics_models contains the physics/model files.

TES_model is a tanh model of a TES transition.
Could be replaced with a different model:
TES should always have basic API for getting R, alphaI, betaI, etc.

DET_model puts one or more TES_models object on an island.

NSE_model takes a DET_model and constructs a noise model for a TDM readout.

MCE_model simulates UBC's MCE as SPIDER uses it.


After downloading, add the path to physics_models to your PYTHONPATH.
